# OpenWifiThermostat

OpenWifiThermostat est un ensemble permettant de créer un thermostat
d'ambiance multizone à bas coûts modulable et personnalisable.

![WebUI](doc/img/server/thumbs.png)

[Catures WebUI](doc/img/server/)

Il permet une programmation hebdomadaire (spécifique à chaque zone), une
administation à distance ou de son cannapé.

L'interface de gestion se fait via une interface web optimisée pour les
smartphones et les tablettes ([en jQuery UI](http://jqueryui.com/)), une
api type REST est égallement exploitable.

Les différents éléments du systèmes communiquent entre eux à travers le
réseau. Ils peuvent donc être facillement déportés.

**!!! AVERTISSEMENT !!!**<br>
Cette application est en cours de développement, les développeurs et
contributeurs ne pourront être tenu pour responsable lors d'un
dysfonctionnement (pouvant par exemple entrainer une panne de votre
chauffage ou l'explosion de votre facture de chauffage, un viellissement
prématuré de votre installation de chauffage ...).


## Les composants du système :


### Le serveur (server):

Le serveur est l'élément centrale, il peut tourner sur un PC (window et
macOSX non testés) soit sur un nano PC (type raspberry).

La gestion est faite par zones (ex le salon, la chambre ...). Chaque
zone peut recevoir la température de un ou plusieurs capteurs et être
connectée à un ou plusieurs pilotes.

[Le serveur](doc/server.md)


### Les capteurs (sensors)

Les capteurs servent principallement à mesurer la température (mais
peuvent égallement transmettre l'humidité et la pression).

Ils peuvent être installés sur le serveur ou être déportés. La plupart
des capteurs déportés se base sur l'esp 8266 une carte programmable très
bas prix avec module wifi intégré.


[Liste des capteurs](doc/sensors.md) (partie logiciel et éléctronique)


### Les pilotes (pilots)

Les pilotes vont controler générallement des relais qui activeront une
chaudière, une électrovanne ...

Comme les capteurs ils peuvents être soit installés sur le serveur soit
déportés.

Si plusieurs zones sont connectées à un même pilote (ex le pilotes de la
chaudière) il suffit qu'une seule zone fasse la demande que le pilotes
soit allumés.

[Liste des pilots](doc/pilots.md)


## Exemples d'installation


### Installations types

Installation pour 2 zones avec un chaudière centrale

Matériel (45-70€) :

  * 1 ou plusieurs modules capteurs wifi (ex: [esp8266-am3201](doc/sensors_esp8266-am3201.md))
    par zone (8-15€ pièce)
  * un serveur (un raspeberry Pi Zero 9€ + une clef wifi 10€) ou un
    autre ordinateur PC tournant 24h/24
  * 1 pilots (ex: [esp8266-gpio](doc/pilots.md)) sur le fil pilot de la
    chaudière (6-12€)
  * idem sur les electrovannes

Configuration

  1. Créez la zone 1, connectez le/les capteur(s) pour la zone 1, le
     pilote de l'électrovanne 1 et celui de la chaudière.
  2. Créez la zone 2, connectez le/les capteur(s) pour la zone 2, le
     pilote de l'électrovanne 2 et celui de la chaudière.


### Installation minimal

Installation pour une zone avec chaudière ventouse.

Matériel (28-30€) :

  * Un serveur (un raspeberry Zero 9€ + adaptateurs 5€ + une clef wifi
    10€)
  * Un capteur de temérature (3-6€) directement branché sur le GPIO du
    raspberry
  * Un module relais pour raspeberry (2-5€) directement branché sur le
    GPIO du raspberry

Configuration

  1. Créer la zone, connecter le/les capteur(s) et le pilote de la
     chaudière.


## Extras


### Impression 3D

Retrouvez différents modèles à imprimer en 3D en rapport avec ce projet
sur [Thingiverse](http://www.thingiverse.com/groups/openwifithermostat/things)


### Software

Retrouvez différentes applications en rapport avec ce projet [extras](doc/extras.md)



## Licences

OpenWifiThermostat est distribué graduitement selon les termes de la GNU
General Public License version 2 ou supérieur publiée par [la Free
Software Foundation](http://www.gnu.org).

L'interface web utilise des graphiques [highcharts/highstock](http://www.highcharts.com/)
dans le cadre d'une diffusion non commercial ils peuvent être utilisés
gratuitement ([licence highcharts](http://shop.highsoft.com/highcharts.html),
[licence highstock](http://shop.highsoft.com/highstock.html).


## Support

[Documentation](doc)

Aide : contactez-moi via IRC (SnouF sur freenode générallement sur #mythtv-fr)
ou via le [gestionnaire de ticket de gitlab](https://gitlab.com/snouf/openwifithermostat/issues).
