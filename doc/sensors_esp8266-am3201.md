# OpenWifiThermostat - esp8266-am2301

Capteur de température et d'humidité.

  * Précision : +/-0.5°C résolution 0.1°C; +/-3% résolution 0.1%
  * Alimentation : 4.0-5.25V via usb (ex: chargeur de téléphone)
  * Consomation : 0.08 A

Les valeurs du capteur peuvent être consulté directement via http://<ip>

La programmation de ce montage ne require pas de matériel particulier.

Limitation : actuellement ne support que la protection WEP et le DHCP.


## Liste du matériel

  * 1x esp8266-01
  * 1x am3201
  * 1x usb-uart (ex: CP2102 MICRO USB to UART TTL Module 6Pin Serial
    Converter STC) ou un programateur pour esp8266.
  * 1x regulateur 3.3V (ex: AMS1117-3.3)
  * 1x cable USB (programmation + allimentation)
  * 1x plaque d'essaie ou le PCB esp8266-am2301 (cf [sources kicad])


## Montage

![Schéma][schéma]
[sources kicad]


## Boîtier

[Imprimable en 3d](http://www.thingiverse.com/thing:1209479)


## Programmation

  1. Ouvrez [esp8266-am2301.ino] dans l'IDE Arduino
  2. Ajoutez le support de l'esp8266 et les libs DHT et ArduinoJson.
  3. Sur votre montage pontez GPIO0 et GND, branchez votre montage via
     usb, enlevez le pontage
  4. Envoyez le code depuis l'IDE Arduino.


## Configuration

Branchez votre montage via USB, ouvrez un terminal série. Les commandes
devront se terminer par une nouvelle ligne (`\n`).

  * Connection au wifi : `connect <ssid> <password>`
  * Changer le nom : `set <name>`
  * D'autre commandes : `help`


[esp8266-am2301.ino]:(sensors/arduino/esp8266-am2301.ino)
[sources kicad]:(hardwares/electronics/esp8266-01%20+%20microusb%20+%20onewire%20plug/kicad/)
[schéma]:(img/sensors/esp8266_onwire.png)
