
# Openwifithermostat - server - Api type REST

OpenWifiThermostat dispose d'une api de type REST.


## Documentation

Les valeurs en entrées et en sorties sont en JSON.


### /api/v1.0/away

  * `GET` : retourne la période d'absence 
  * `PUT {from:TIMESTAMP}` : modifie le début de la période d'absence 
  * `PUT {to:TIMESTAMP}` : modifie la fin de la période d'absence 


### /api/v1.0/datetime

  * `GET` : retourne la date (timestamp) 


### /api/v1.0/help

  * `GET` : titres, unitée des différentes clefs utilisées 


### /api/v1.0/history

  * `GET` : retourne l'historique ?from:TIMESTAMP : à partir de (par
défaut il y a une semaine) &to:TIMESTAMP : jusqu'à (par défaut
aujourd'hui) &cols:temp,hrel,pressure,power : les valeurs de (par défaut
toutes) &zones:1,2,.. : id(s) des zones (séparateur "," par défaut
toutes) &average:[daily|houry|none] : moyenne par journées, heures ou
aucune automatique par défaut (>60j: 'daily', >7j: 'houry', <=7j: 'none') 


### /api/v1.0/modes

  * `GET` : retourne les modes 
  * `POST {name=STR}` : ajout un mode ("unnamed" par defaut) 


### /api/v1.0/modes/<int:mode_id>

  * `<int:mode_id>` : l'id du mode 
  * `DELETE` : supprime le mode 
  * `GET` : le mode 
  * `PUT {name:STR}` : modifie le nom du mode 


### /api/v1.0/modes/active

  * `GET` : le mode actif 
  * `PUT {id:INT}` : définit le mode actif 
  * `PUT {name:STR}` : modifie le nom du mode actif 


### /api/v1.0/pilots

  * `GET` : les pilotes toutes zones confondues 
  * `POST {location:STR}` : ajoute le pilote (location REQUIS adresse du
pilote `openwifithermostat.pilot...`) 
  * `POST {name:STR}` : definit le nom du pilote. Si vide ou absent,
récupéré sur le pilote 


### /api/v1.0/pilots/<int:pilot_id>

  * `<int:pilot_id>` : l'id du pilote 
  * `DELETE` : supprime le pilote et le détache de toutes les zones 
  * `PUT {name}` : modifie le nom si vide ou absent, récupéré sur le
pilote 


### /api/v1.0/sensors

  * `GET` : les capteurs toutes zones confondues 
  * `POST {location:STR}` : ajoute le capteur (location REQUIS adresse du
capteur `openwifithermostat.sensor...`). 
  * `POST {name:STR}` : définit nom du capteur. Si vide ou absent,
recupéré sur le capteur 


### /api/v1.0/sensors/<int:sensor_id>

  * `<int:sensor_id>` : l'id du capteur 
  * `DELETE` : supprime le capteur et le détache de toutes les zones 
  * `PUT {name}` : modifie le nom du capteur 


### /api/v1.0/udp/devices/<path:location_start>

  * `<path:location_start>` : un début d'adresse 
  * `GET` : les périphériques (capteurs, pilotes) commencant par cette
adresse 


### /api/v1.0/zones

  * `GET` : liste des zones 
  * `POST {name=STR}` : ajout une zone (unnamed par défaut) 


### /api/v1.0/zones/<int:zone_id>

  * `<int:zone_id>` : l'id de la zone 
  * `DELETE` : supprime la zone 
  * `GET` : la zone 
  * `PUT {name:STR}` : modifie le nom de la zone 


### /api/v1.0/zones/<int:zone_id>/pilots

  * `<int:zone_id>` : l'id de la zone 
  * `GET` : les pilotes de cette zone 
  * `POST {location:STR}` : ajoute le pilote (location REQUIS adresse du
pilote `openwifithermostat.pilot...`) 
  * `POST {logic_reverse}` : définit l'inversion de logique (sur marche
si la température est atteinte) (False par defaut) 
  * `POST {name}` : définit nom du pilot (si non spécifié ou vide
recupérer sur le pilote) 


### /api/v1.0/zones/<int:zone_id>/pilots/<int:pilot_id>

  * `<int:pilot_id>` : l'id du pilote 
  * `<int:zone_id>` : l'id de la zone 
  * `DELETE` : supprime le pilote pour cette zone 
  * `PUT {logic_reverse}` : modifie l'inversion de logique 
  * `PUT {name}` : modifie le nom du pilote (si non spécifié ou vide
récupérer sur le pilots) 


### /api/v1.0/zones/<int:zone_id>/schedules/now

  * `<int:zone_id>` : l'id de la zone 
  * `GET` : la date (timestamp), la plage de programmation en cours (
début, fin en timestamp), les températures (voulue actuellement,
prochaine en °C) 
  * `PUT {temp:{now:FLOAT}` : modifie provisoirement la température de la
plage de programmation actuelle 
  * `PUT {time:{end:TIMESTAMP}}` : modifie provisoirement la fin de la
plage de programmation actuelle 


### /api/v1.0/zones/<int:zone_id>/schedules/weekly

  * `<int:zone_id>` : l'id de la zone 
  * `GET` : la programation hebdomadaire sous la forme d'une liste de
plage de programation [week_second, temp]. week_second: secondes depuis le
début de la semaine (lundi 00:00:00), temp: température en °C.
week_second sera arrondi au 1/4 d'heure. 
  * `POST {week_second:FLOAT,temp:FLOAT}` : ajoute la plage (si
week_second existe remplace l'ancienne plage) 


### /api/v1.0/zones/<int:zone_id>/schedules/weekly/<int:week_seconds>

  * `<int:week_seconds>` : second de la semaines 
  * `<int:zone_id>` : l'id de la zone 
  * `DELETE` : supprime la plage de programmation 
  * `PUT {week_second:FLOAT,temp:FLOAT}` : modifie la plage de
programmation 


### /api/v1.0/zones/<int:zone_id>/sensors

  * `<int:zone_id>` : l'id de la zone 
  * `GET` : les capteurs de cette zone 
  * `POST {location:STR}` : ajoute le capteur (location REQUIS adresse du
capteur `openwifithermostat.sensor...`) 
  * `POST {name}` : définit le nom du capteur (si non spécifié ou vide
recupéré sur le capteur) 
  * `POST {weight}` : définit la pondération pour la moyenne (par
défaut 1) 


### /api/v1.0/zones/<int:zone_id>/sensors/<int:sensor_id>

  * `<int:sensor_id>` : l'id du capteur 
  * `<int:zone_id>` : l'id de la zone 
  * `DELETE` : supprime le capteur pour cette zone 
  * `PUT {name}` : modifie nom du capteur (si non spécifié ou vide
récupérer sur le capteur) 
  * `PUT {weight}` : modifie la pondération 

## unitées des variables

| Champs        | Unitée   | Description                               |
|---------------|----------|-------------------------------------------|
| `hrel`        | %        | Humiditée relative                        |
| `pressure`    | °hPa     | Pression atmosphérique                    |
| `temp`        | °C       | Température                               |
| `TIMESTAMP`   | secondes | Les dates                                 |
| `week_second` | secondes | Nombre de secondes depuis lundi 00:00:00  |
