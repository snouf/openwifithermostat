# Openwifithermostat - Serveur (server)


## Pérequis

Installez:
  - python-flask (>=0.10) `pip install Flask --upgrade`
  - git


## Installation

Lancez `install.py` qui va créer la base de donnée.


## Mise à jours

Faites un sauvegarde de la base de donnée `db.sqlite` et lancez
`update.py` qui s'occuperra des différentes opérations de mise à jours.


## Lancement

Lancer `start_server.py`


## Configuration

  1. Rendez vous sur `http://IP_SERVER:5000`
  2. Créez votre 1ère zone (menu → ajouter une zone)
  3. Connectez les capteurs et pilotes de cette zone


## Documentations

  * [api type REST](doc/server_api_REST.md)
  * [Base de données](doc/bdd.md)
