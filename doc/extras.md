# Openwifithermostat - Extras


Les extras sont de petits outils ou gadjets développés autours
d'Openwifithermostat.

| Nom                    | Commentaire                                 |
|------------------------|---------------------------------------------|
| [TelegramBot]          | Bot [Telegram](http:://telegram.org) permettant de controler votre installation. |

[telegramBot]:extras/telegramBot/


