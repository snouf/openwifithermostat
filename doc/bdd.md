#OpenWifiThermosat - server - Base de données


Le serveur de OpenWifiThermosat utilise une base de donner au format
sqlite.


## Structure

Voir [install.py](../server/install.py).


## Maintenance

Il peut être utile de de faire quelque tâche de maintenance.


### Compresser la base

```sql
vacuum
```


### Vérifier qu'il n'y a pas doubons

```sql
select count(*) as nb_record, datetime, zone_id, temp, hrel, pressure, power
from history_daily
group by datetime, zone_id, temp, hrel, pressure, power
having count(*) > 1;

select count(*) as nb_record, datetime, zone_id, temp, hrel, pressure, power
from history_daily
group by datetime, zone_id, temp, hrel, pressure, power
having count(*) > 1;

select count(*) as nb_record, datetime, zone_id, temp, hrel, pressure, power
from history_daily
group by datetime, zone_id, temp, hrel, pressure, power
having count(*) > 1;
```


### Supprimer les éventuelles doublons

```sql
delete from history_daily
where rowid not in
(
select min(rowid)
from history_daily
group by datetime, zone_id, temp, hrel, pressure, power
);

delete from history_houry
where rowid not in
(
select min(rowid)
from history_houry
group by datetime, zone_id, temp, hrel, pressure, power
);

delete from history
where rowid not in
(
select min(rowid)
from history
group by datetime, zone_id, temp, hrel, pressure, power
);
```
