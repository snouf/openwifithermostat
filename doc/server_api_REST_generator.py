#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import re

in_route = False
in_comment = False

doc = {}
arg = ''

with open("../server/web_ui.py") as f:
    for line in f.readlines() :
        if in_comment :
            if re.search(r"'''",line) :
                in_route = False
                in_comment = False
                arg = ''
            else :
                match = re.search(r"^\s*(GET|POST.*{+.*}+|PUT.*{+.*}+|DELETE|<.*>).*:(.*)",line)
                if match:
                    arg = match.group(1)
                    line = match.group(2)

                if arg not in doc[url] :
                    doc[url][arg] = ''
                if doc[url][arg].strip() != line.strip() :
                    doc[url][arg] += line.strip()+" "
        elif in_route :
            if re.search(r"'''",line) :
                in_comment = True
        else :
            route = re.search(r"^@app.route\(['|\"](/api/v1.0/[^'\"]*).*\[([^\]]*)",line)
            if route :
                url = route.group(1)
                if url not in doc :
                    doc[url]={}
                in_route = True


print '''
# Openwifithermostat - server - Api type REST

OpenWifiThermostat dispose d'une api de type REST.


## Documentation

Les valeurs en entrées et en sorties sont en JSON.'''

doc = doc.items()
doc.sort()
for title,args in doc :
    print "\n\n### %s\n"%title
    args = args.items()
    args.sort()
    for arg,desc in args :
        if arg == '' :
            s = "%s"%desc
        else :
            s = "  * `%s` : %s"%(arg,desc)
        while len(s) > 75 :
            pos = s.rfind(' ',0,75)
            print s[0:pos]
            s = s[pos+1:]
        print s

print '''
## unitées des variables

| Champs        | Unitée   | Description                               |
|---------------|----------|-------------------------------------------|
| `hrel`        | %        | Humiditée relative                        |
| `pressure`    | °hPa     | Pression atmosphérique                    |
| `temp`        | °C       | Température                               |
| `TIMESTAMP`   | secondes | Les dates                                 |
| `week_second` | secondes | Nombre de secondes depuis lundi 00:00:00  |'''

