# Openwifithermostat - Pilotes (pilots)

| Nom                     | Prix             | Commentaire      |
| ----------------------- | ---------------- | ---------------- |
| [raspberrypi-gpio] :+1: | 3-10€ (hors rpi) | Board relais simplement brancher sur les GPIOs |
| [raspberrypi-virtual]   | 0€               | Pilote pour test, affiche l'activité dans un terminal, fonctionne sur n'importe quel PC |

[raspberrypi-gpio]:pilots/python/
[raspberrypi-virtual]:pilots/python/
