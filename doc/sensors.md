#OpenWifiThermosat - Capteurs (sensors)

| Nom                   | Température | Humiditée  | Pression   | Prix       | Commentaire         |
| --------------------- | ----------- | ---------- | ---------- | ---------- | ------------------- |
| [esp8266-am2301] :+1: | +/- 0.5 °C  | +/- 3%     | non        | 6-20€      | Basé sur une carte wifi esp8266 et un capteur am2301. Inclu un montage électronique. |
| [openweathermap]      | oui         | oui        | oui        | 0€         | Récupère les informations sur openweathermap (par ex pour la température extérieur).  |
| [raspberrypi-am2301]  | +/- 0.5 °C  | +/- 3%     | non        | 4-10€ (*)  | Am2301 simplement branché sur les GPIOs d'un Raspberry Pi. |
| [virtual]             | oui         | oui        | oui        | 0€         | Pour des tests (retourne des valeurs aléatoires). |

(*): hors prix du raspberrypi

[esp8266-am2301]:sensors/arduino/esp8266-am2301/
[openweathermap]:sensors/python/
[raspberrypi-am2301]:sensors/python/
[virtual]:sensors/python/
