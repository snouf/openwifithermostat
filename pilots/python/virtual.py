#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import _basePilot

_basePilot.end_uid = 'virtual'
_basePilot.parser.set_defaults(name='Virtual')
_basePilot.parser.set_description("A virtual pilot (display status on terminal)")


def set_power(power) :
    _basePilot._power = power
    print 'Power set to',_basePilot._power
    return _basePilot._power
_basePilot.set_power = set_power


if __name__ == '__main__' :
    _basePilot.init()
