#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import _basePilot
import RPi.GPIO as GPIO


_basePilot.parser.set_defaults(name='Raspberry Pi GPIO')
_basePilot.parser.set_description("A pilot for raspberryPi GPIO control")
_basePilot.parser.add_option("-g", "--gpio", dest="gpio",type="int", help="[REQUIRED] GPIO pin")
_basePilot.parser.add_option("-r", "--reverse", dest="reverse",action="store_true", help="GPIO to LOW when power is on")


def set_power(power):
    if (not power and _basePilot.opts.reverse) or (power and not _basePilot.opts.reverse):
        GPIO.output(opts.gpio, GPIO.HIGH)
    else :
        GPIO.output(opts.gpio, GPIO.LOW)
    _basePilot._power = power
    return _basePilot._power
_basePilot.set_power = set_power



if __name__ == '__main__' :
    (opts, args) = _basePilot.parser.parse_args()
    if not opts.gpio :
        _basePilot.parser.error('GPIO require')

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(opts.gpio, GPIO.OUT)

    _basePilot.end_uid = 'raspberry.gpio%i'%opts.gpio
    _basePilot.init()
