#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


from warnings import warn
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from optparse import OptionParser
from sys import stdout
from threading import Timer
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST, gethostbyname, gethostname
from uuid import getnode as get_mac
from json import loads, dumps


class BasePilotHandler(BaseHTTPRequestHandler):

    def __init__(self,*args):
        BaseHTTPRequestHandler.__init__(self,*args)


    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()


    def do_GET(self):
        if self.path == '/' :
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            with open(dirname(__file__)+'/template.html', 'r') as f:
                self.wfile.write(f.read())
            f.closed
        elif self.path == '/api/v1.0' :
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(dumps({
                "name":opts.name,
                "power":get_power()
            }))
        else :
            self.send_error(404)


    def do_POST(self) :
        if self.path == '/api/v1.0' :
            data = loads(self.rfile.read(int(self.headers['Content-Length'])))
            t = type(data["power"])
            if t == bool :
                pass
            elif t == str :
                if data["power"].lower() in ('on', 'true', '1', 'high') :
                    data["power"] = True
                elif data["power"].lower() == ('off', 'false', '0', 'low') :
                    data["power"] = False
            elif t == int :
                if data["power"] == 1 :
                    data["power"] = True
                elif data["power"] == 0 :
                    data["power"] = False
            elif t == float :
                if data["power"] == 1. :
                    data["power"] = True
                elif data["power"] == 0. :
                    data["power"] = False
            else :
                warn("Can't parse date %s to power [bool]"%data)
                return

            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(dumps({'power':_set_power(data["power"])}))


opts = None
args = None
end_uid = 'base'
api_url = 'http://<ip>:%i/api/'
api_version = ['1.0']
_power = None
httpHandler = BasePilotHandler


def _set_power(power) :
    '''
    Toggle power pilot [bool] return new power
    '''
    global cut_on_no_comm
    cut_on_no_comm.cancel()
    cut_on_no_comm = Timer(30, _set_power, [False])
    cut_on_no_comm.start()
    return set_power(power)


def set_power(power) :
    '''
    Toggle power pilot [bool] return new power
    '''
    global _power
    _power = power
    return _power


def get_power() :
    '''
    Toggle power pilot [bool] return new power
    '''
    return _power


def parse_args():
    global opts, args
    (opts, args) = parser.parse_args()
    return (opts, args)


def start_broadcast() :
    data = dumps({
        'name':opts.name,
        'power':get_power(),
        'api_url':api_url%opts.port,
        'api_version':api_version,
    })
    s.sendto("openwifithermostat.pilot.%s.%s=%s"%(mac,end_uid,data), ('<broadcast>', 5000))
    Timer(5, start_broadcast).start()


def start_httpserver() :
    httpd = HTTPServer(("", opts.port), httpHandler)
    print "Start http server %s at %s:%i"%(opts.name,ip,opts.port)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()


def init():
    parse_args()
    _set_power(_power)
    start_broadcast()
    start_httpserver()


parser = OptionParser(
    description="Base pilot for openWifiThermostat"
)
parser.add_option("-n", "--name", dest="name",default="base pilot", type="string", help="pilot name [default: %default]")
parser.add_option("-p", "--port", dest="port",default=8082, type="int", help="port http [default: %default]")

cut_on_no_comm = Timer(30, _set_power, [False]) #coupe le relay si aucune communication
cut_on_no_comm.start()

s = socket(AF_INET, SOCK_DGRAM) #create UDP socket
s.bind(('', 0))
s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1) #this is a broadcast socket
ip= gethostbyname(gethostname())
mac = ':'.join(("%012X" % get_mac())[i:i+2] for i in range(0, 12, 2))


if __name__ == '__main__' :
    init()
