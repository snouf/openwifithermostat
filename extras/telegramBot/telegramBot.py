#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


from optparse import OptionParser
import sys, time, requests, math, re
import telepot
from pprint import pprint

reload(sys)
sys.setdefaultencoding("utf-8")

parser = OptionParser(description="OpenWifiThermostat Bot")
parser.add_option("-t", "--token", dest="token", type="string", help='Token pour l\'accès à l\'api http')
parser.add_option("-u", "--users", dest="users", type="string", default="", help='Liste des ids utilisateurs authorisé à lancer des commandes (séparteur ","')
parser.add_option("-s", "--server", dest="server", type="string", default="http://localhost:5000", help='Url de serveur (http://localhost:5000 par défaut)')
(opts, args) = parser.parse_args()

users = []
for user in opts.users.split(',') :
    users.append(int(user))


bot = telepot.Bot(opts.token)
pprint(bot.getMe())

api_url = opts.server+'/api/v1.0/'

users_action = {}

def human_readable_delta_t (delta_t) :
    delta_t += 5
    d = math.floor(delta_t/60/60/24)
    h = math.floor(delta_t/60/60-d*24)
    m = math.floor(delta_t/60-d*60*24-h*60)
    ret = ''
    if d > 1 : ret+= '%i jours '%d
    elif d == 1 : ret+= '1 jour '
    if h : ret+= '%i heures '%h
    elif h == 1 : ret+= '1 heure'
    if m : ret+= '%i minutes '%m
    elif m == 1 : ret+= '1 minute'
    if not d and not h and not m :
        ret+= 'quelques secondes'
    return ret

def handle_message(msg):
    pprint(msg)
    if msg['from']['id'] in users:
        if not users_action.has_key(msg['from']['id']) :
            users_action[msg['from']['id']] = {
                'cmd':'',
                'actions':{},
                'zone':{}
            }

        if msg['text'] == 'Annuler':
            if users_action[msg['from']['id']]['cmd'] in ('edit_prog_temp_now', 'edit_prog_time_end') :
                users_action[msg['from']['id']]['cmd'] = 'edit_prog'
            else :
                users_action[msg['from']['id']]['cmd'] = ''

        if msg['text'] in ['Acceuil', '/start', 'Retourner à l\'acceuil'] :
            bot.sendMessage(
                msg['from']['id'],
                'Bienvenue %s, que souhaitez vous faire ?'%msg['from']['first_name'],
                reply_markup={'keyboard': [
                    ['Afficher les températures'],
                    ['Changer de mode'],
                    ['Modifier la programation'],
                ]}
            )
            users_action[msg['from']['id']]['cmd'] = ''
        elif msg['text'] == 'Afficher les températures' :
            r = requests.get(api_url+'zones')
            txt = 'Voici les températures actuelles'
            for z in r.json()['zones'] :
                txt += '\n - %(name)s : %(temp).1f°C (%(hrel).1f%%)'%z
            bot.sendMessage(
                msg['from']['id'],
                txt,
                reply_markup={'keyboard': [
                    ['Afficher les températures'],
                    ['Retourner à l\'acceuil']
                ]}
            )
        elif msg['text'] == 'Changer de mode' :
            r = requests.get(api_url+'modes/active')
            active_mode = r.json()['name']
            r = requests.get(api_url+'modes')
            users_action[msg['from']['id']]['modes'] = {}
            buttons = []
            i = 1
            for m in r.json()['modes'] :
                label = '%i. %s'%(i,m["name"])
                users_action[msg['from']['id']]['actions'][label] = m["id"]
                buttons.append([label])
                i += 1
            buttons.append(['Retourner à l\'acceuil'])
            users_action[msg['from']['id']]['cmd'] = 'change_mode'
            bot.sendMessage(
                msg['from']['id'],
                'Le mode actuels est "%s".\nSélectionnez un mode ou retournez à l\'acceuil'%active_mode,
                reply_markup={'keyboard':buttons}
            )
        elif msg['text'] == 'Modifier la programation' :
            r = requests.get(api_url+'zones')
            users_action[msg['from']['id']]['cmd'] = 'edit_prog'
            users_action[msg['from']['id']]['zone']['id'] =  None
            buttons = []
            i = 1
            for z in r.json()['zones'] :
                label = '%i. %s'%(i,z["name"])
                buttons.append([label])
                users_action[msg['from']['id']]['actions'][label] = z
                i += 1
            buttons.append(['Retourner à l\'acceuil'])
            bot.sendMessage(
                msg['from']['id'],
                'Vous voulez modifier la programation de quelle zone ?',
                reply_markup={'keyboard': buttons}
            )
        elif users_action[msg['from']['id']]['cmd'] == 'change_mode' :
            if msg['text'] in users_action[msg['from']['id']]['actions'].keys() :
                r = requests.put(
                    api_url+'modes/active',
                    json={'id':users_action[msg['from']['id']]['actions'][msg['text']]}
                )
                if r.ok :
                    bot.sendMessage(
                        msg['from']['id'],
                        'Le mode "%s" est maintenant actif'%r.json()['name']
                    )
                else :
                    bot.sendMessage(
                        msg['from']['id'],
                        'Une erreur est survenue lors du changement de mode',
                        reply_markup={'keyboard': [
                            ['Retourner à l\'acceuil']
                        ]}
                    )
            else :
                bot.sendMessage(
                    msg['from']['id'],
                    'Je n\'ai pas compris, Entrez à nouveau le mode'
                )
        elif users_action[msg['from']['id']]['cmd'] == 'edit_prog' :
            if msg['text'] == 'Changer la température' :
                bot.sendMessage(
                    msg['from']['id'],
                    'Entrez la nouvelle température',
                    reply_markup={'keyboard': [
                        users_action[msg['from']['id']]['preselect_temp'],
                        ['Annuler']
                    ]}
                )
                users_action[msg['from']['id']]['cmd'] = 'edit_prog_temp_now'
            elif msg['text'] == 'Changer la durée' :
                bot.sendMessage(
                    msg['from']['id'],
                    'Entrez la nouvelle durée',
                    reply_markup={'keyboard': [
                        ['1','5','10'],
                        ['15','30','1h'],
                        ['2h','6h','1j'],
                        ['Annuler']
                    ]}
                )
                users_action[msg['from']['id']]['cmd'] = 'edit_prog_time_end'
            else : #dialog choix
                if msg['text'] != 'Annuler' :
                    users_action[msg['from']['id']]['zone']['id'] =  None
                    if users_action[msg['from']['id']]['actions'].has_key(msg['text']) :
                        users_action[msg['from']['id']]['zone'] = users_action[msg['from']['id']]['actions'][msg['text']]
                if users_action[msg['from']['id']]['zone']['id'] :
                    r = requests.get (api_url+'zones/%i/schedules/now'%users_action[msg['from']['id']]['zone']['id'])
                    j = r.json()
                    delta_t = j['time']['end'] - j['now']
                    users_action[msg['from']['id']]['preselect_temp'] = []
                    users_action[msg['from']['id']]['now'] = j['now']
                    if j['temp']['now'] != j['temp']['normal_now'] :
                        users_action[msg['from']['id']]['preselect_temp'].append('%.1f'%j['temp']['normal_now'])
                    if j['temp']['now'] != j['temp']['next'] :
                        users_action[msg['from']['id']]['preselect_temp'].append('%.1f'%j['temp']['next'])

                    bot.sendMessage(
                        msg['from']['id'],
                        'Actuellement la température de programation de la zone "%s" est de %.1f°C et prend fin dans %s'%(users_action[msg['from']['id']]['zone']['name'], j['temp']['now'],human_readable_delta_t(delta_t)),
                        reply_markup={'keyboard': [
                            ['Changer la température'],
                            ['Changer la durée'],
                            ['Retourner à l\'acceuil']
                        ]}
                    )
                else :
                    bot.sendMessage(
                        msg['from']['id'],
                        'Zone non valide'
                    )
        elif users_action[msg['from']['id']]['cmd'] == 'edit_prog_temp_now' :
            try:
                temp = float(msg['text'])
            except :
                print 'except'
                bot.sendMessage(
                    msg['from']['id'],
                    'Je n\'ai pas compris, Entrez à nouveau une température'
                )
            else :
                r = requests.put(
                    api_url+'zones/%i/schedules/now'%users_action[msg['from']['id']]['zone']['id'],
                    json={'temp':{'now':temp}}
                )
                j = r.json()
                bot.sendMessage(
                    msg['from']['id'],
                    'Nouvelle température %.1f°C'%j['temp']['now'],
                    reply_markup={'keyboard': [
                        ['Changer la température'],
                        ['Changer la durée'],
                        ['Retourner à l\'acceuil']
                    ]}
                )
                users_action[msg['from']['id']]['cmd'] = 'edit_prog'
        elif users_action[msg['from']['id']]['cmd'] == 'edit_prog_time_end' :
            try:
                d,h,m = re.match(r'^(?:([0-9]*)j)?\s*(?:([0-9]*)h)?\s*([0-9]*)$',msg['text']).groups()
            except :
                bot.sendMessage(
                    msg['from']['id'],
                    'Je n\'ai pas compris, Entrez à nouveau la durée'
                )
            else :
                time_delta = 0.
                print d,h,m
                if d : time_delta += float(d) * 60 * 60 * 24
                if h : time_delta += float(h) * 60 * 60
                if m : time_delta += float(m) * 60
                t_end = users_action[msg['from']['id']]['now'] + time_delta
                r = requests.put(
                    api_url+'zones/%i/schedules/now'%users_action[msg['from']['id']]['zone']['id'],
                    json={'time':{'end':t_end}}
                )
                j = r.json()
                bot.sendMessage(
                    msg['from']['id'],
                    'La programmation se termine maintenant dans %s'%human_readable_delta_t(j['time']['end']-j['now']),
                    reply_markup={'keyboard': [
                        ['Changer la température'],
                        ['Changer la durée'],
                        ['Retourner à l\'acceuil']
                    ]}
                )
                users_action[msg['from']['id']]['cmd'] = 'edit_prog'
        else :
            bot.sendMessage(
                msg['from']['id'],
                'Je n\'ai pas compris',
                reply_markup={'keyboard': [
                    ['Retourner à l\'acceuil']
                ]}
            )
    else :
        bot.sendMessage(msg['from']['id'], 'Désolé vous n\'avez pas accès à ce bot')

bot.notifyOnMessage(handle_message)
while 1:
    time.sleep(10)
