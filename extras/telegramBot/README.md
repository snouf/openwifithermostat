# OpenWifiThermostat - TelgramBot

Controler votre installation depuis un compte Telegram.


## Prérequis

  * Installez [telepot](https://github.com/nickoala/telepot)
  * [Créer votre bot](https://core.telegram.org/bots)


## Utilisation

Lancez votre bot

`./telegramBot.py -t ... -u ... -s ...`
  * `-t` : token donné par @BotFather
  * `-o` : users id dont l'accès est autorisé (séparateur ",")
  * `-s` : url du serveur (http://localhost:5000 par défaut)

Puis dialoguer avec votre bot (des boutons remplace le clavier)





