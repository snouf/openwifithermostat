#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


if __name__ == '__main__':
    import __init__
    exit()

import logging
import DB, Cfg


class Modes (list) :
    '''
    Liste des modes
      object active: le mode actif
    '''

    def __init__(self) :
        DB.c.execute("SELECT id FROM modes")
        rows = DB.c.fetchall()
        if len(rows) == 0 :
            '''Aucune zone existe en créer une'''
            self.add('Default')
        for row in rows :
            mode = Mode().load(row[0])
            self.append(mode)
            if mode.id == Cfg.mode_id :
                self.active = mode


    def get(self,id) :
        '''
        Cherche un pilot
          int id: id du pilot
        Return: object Pilot
        '''
        for mode in self:
            if mode.id == id:
                return mode
        return None


    def get_all(self) :
        return self


    def add(self, name) :
        '''
        Ajout un mode
          str name: nom du pilot
        Return: object Modes
        '''
        mode = Mode().new(name=name)
        self.append(mode)
        return mode


    def delete(self,id) :
        '''
        Supprime le mode
          int id: id du mode à supprimer
        Return: list modes
        '''
        if id != 1 :
            '''on ne peut pas supprimer le mode 1'''
            mode = self.get(id)
            DB.c.execute("DELETE FROM schedules WHERE mode_id = ?", [id])
            DB.c.execute("DELETE FROM modes WHERE id = ?", [id])
            DB.conn.commit()
            self.remove(mode)
            if id == self.active.id :
                self.set_active(1)
        return self.active


    def set_active(self,id) :
        '''
        Active le mode
          int id: id du mode à activer
        Return: Mode actif
        '''
        for mode in self :
            print mode.id ,'==', id
            if mode.id == id :
                self.active = mode
                Cfg.set('mode_id',id)
                return mode
        return None


    def get_active(self) :
        '''
        Return: Mode actif
        '''
        return self.active


class Mode :
    '''
    Un Mode
      int id: id
      str name: nom
    '''

    def __init__(self) :
        self.id = None
        self.name = None
        self.power = None


    def new(self, name=None) :
        '''
        Créer un mode
          str name: nom du pilot
        Return: object Mode
        '''
        DB.c.execute("INSERT INTO modes (name) VALUES (?)", [name])
        DB.conn.commit()
        self.id = DB.c.lastrowid
        self.name = name

        #Copie des schedules du mode par défaut dans le nouveau mode pour toute les zones
        DB.c.execute("SELECT id FROM zones")
        for r_zone in DB.c.fetchall():
            DB.c.execute("SELECT week_second, temp FROM schedules WHERE zone_id = ? AND mode_id = 1", [r_zone[0]])
            for s_row in DB.c.fetchall():
                DB.c.execute("INSERT INTO schedules (zone_id, mode_id, week_second, temp) VALUES ( ? , ? , ? , ?)", [r_zone[0], self.id, s_row[0], s_row[1]])
        DB.conn.commit()

        return self


    def load(self, id) :
        '''
        Charge un mode anciennement créer
          int id: id du capteur
        Return: object Pilot
        '''
        self.id = id
        DB.c.execute("SELECT name FROM modes WHERE id = ?", [id])
        self.name = DB.c.fetchone()[0]
        return self


    def set_name(self, name) :
        DB.c.execute("UPDATE modes SET name = ? WHERE id = ?",[name, self.id])
        DB.conn.commit()
        self.name = name
        return self.name

