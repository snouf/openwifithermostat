#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


import subprocess, os, version
from datetime import datetime, time
from database import DB
import version as Version

DB = DB()

version = "0"

if os.path.exists(".git") :
    version += "git"+subprocess.check_output(["git", "log", "--date", "iso-strict", "-1", "--format=%ad"]).strip() + "." + subprocess.check_output(["git", "describe", "always"]).strip()

DB.c.execute("SELECT count(*) FROM sqlite_master WHERE type='table' ")
if DB.c.fetchone()[0] :
    print "Une base de donnée existe déjà"
    exit(1)

print "Création de la base de donnée"
DB.c.executescript("""
CREATE TABLE `config` (
    `key`   TEXT,
    `value` TEXT
);
CREATE TABLE `modes` (
    `id`    INTEGER UNIQUE,
    `name`  TEXT,
    PRIMARY KEY(id)
);
CREATE TABLE `pilots` (
    `id`    INTEGER UNIQUE,
    `name`  TEXT,
    `location`  TEXT,
    `logic_reverse` INTEGER,
    PRIMARY KEY(id)
);
CREATE TABLE `schedules` (
    `week_second`   INTEGER,
    `temp`  NUMERIC,
    `zone_id`   INTEGER,
    `mode_id`   INTEGER
);
CREATE TABLE `sensors` (
    `id`    INTEGER,
    `name`  TEXT,
    `location`  TEXT,
    PRIMARY KEY(id)
);
CREATE TABLE `zones` (
    `id`    INTEGER UNIQUE,
    `name`  TEXT,
    `away_temp`  NUMERIC DEFAULT 12,
    `speed_temp_climb` NUMERIC DEFAULT 0,
    PRIMARY KEY(id)
);
CREATE TABLE `history` (
    `datetime` INTEGER,
    `zone_id`   INTEGER,
    `temp`  NUMERIC DEFAULT NULL,
    `hrel`  NUMERIC DEFAULT NULL,
    `pressure`  NUMERIC DEFAULT NULL,
    `power`  INTEGER DEFAULT NULL
);
CREATE TABLE `history_houry` (
    `datetime` INTEGER,
    `zone_id`   INTEGER,
    `temp`  NUMERIC DEFAULT NULL,
    `hrel`  NUMERIC DEFAULT NULL,
    `pressure`  NUMERIC DEFAULT NULL,
    `power`  NUMERIC DEFAULT NULL
);
CREATE TABLE `history_daily` (
    `datetime` INTEGER,
    `zone_id`   INTEGER,
    `temp`  NUMERIC DEFAULT NULL,
    `hrel`  NUMERIC DEFAULT NULL,
    `pressure`  NUMERIC DEFAULT NULL,
    `power`  NUMERIC DEFAULT NULL
);
CREATE TABLE `zones_pilots` (
    `zone_id`   INTEGER,
    `pilot_id`  INTEGER,
    `logic_reverse` INTEGER DEFAULT 0
);
CREATE TABLE `zones_sensors` (
    `zone_id`   INTEGER,
    `sensor_id` INTEGER,
    `weight`    NUMERIC DEFAULT 1
);
""")
print "Initialisation de la configuration de base"
DB.c.execute("INSERT INTO `config` VALUES ('version', ? );",[Version.version])
DB.c.execute("INSERT INTO `config` VALUES ('db_struct', ? );",[Version.db_struct])
DB.c.execute("INSERT INTO `config` VALUES ('station_name','OpenWifiThermosat');")
DB.c.execute("INSERT INTO `config` VALUES ('away_from',?);",[datetime.fromtimestamp(0)])
DB.c.execute("INSERT INTO `config` VALUES ('away_to',?);",[datetime.fromtimestamp(0)])
DB.c.execute("INSERT INTO `config` VALUES ('mode_id',1);")
DB.c.execute("INSERT INTO `config` VALUES ('default_temp',6.0);")
DB.c.execute("INSERT INTO `config` VALUES ('history_entry_every',?);",[ 15*60 ])#15min
DB.c.execute("INSERT INTO `config` VALUES ('history_save_at',?);",[ 4*60*60 ])#4h
DB.c.execute("INSERT INTO modes (name) VALUES ('Default');")
DB.conn.commit()
print "installation terminée"
