#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.



from datetime import datetime, timedelta
from time import time
from threading import Timer
from math import ceil
import logging
import DB, Cfg


class History (list) :
    '''
    Historique des enregistrements
      datetime dt_houry: heure en cours
      datetime dt_daily: journée en cours
      list houry: moyenne par heur des données non enregistrées.
      list daily: moyenne par jour des données non enregistrées.
      dict houry_[temps|hrel|pressures|powers]: data pour l'heure en
            cours. ([zone_id]=data)
      dict daily_[temps|hrel|pressures|powers]: data pour la journée en
            cours. ([zone_id]=data)
      list zones_ids: les ids de zones présent dans l'historique
      dict next_record: timestamp du le prochaine entrée
            La clef est l'id de la zone)
      dict last_power: dernier "power" (état allumé/éteint) enregistré.
            Le clef est l'id de la zone)
    '''

    def __init__(self) :
        dt_now = datetime.now() #maintenant
        dt_now_houry = datetime(year=dt_now.year, month=dt_now.month, day=dt_now.day, hour=dt_now.hour) #heure en cours
        dt_now_daily = datetime(year=dt_now.year, month=dt_now.month, day=dt_now.day) #journée en cours
        prev_dt_now_houry = dt_now_houry - timedelta(hours=1)
        prev_dt_now_daily = dt_now_houry - timedelta(days=1)

        self.dt_houry = dt_now_houry
        self.dt_daily = dt_now_daily
        self.houry = []
        self.daily = []
        self.houry_temps = {}
        self.houry_hrels = {}
        self.houry_pressures = {}
        self.houry_powers = {}
        self.daily_temps = {}
        self.daily_hrels = {}
        self.daily_pressures = {}
        self.daily_powers = {}
        self.zones_ids= []
        self.next_record = {}
        self.last_power = {}

        now = datetime.now()

        DB.c.execute("SELECT MIN(datetime) AS '[timestamp]' FROM history")
        self.dt_fist_record = DB.c.fetchone()[0]
        DB.c.execute("SELECT MAX(datetime) AS '[timestamp]' FROM history")
        dt_last_record = DB.c.fetchone()[0]
        DB.c.execute("SELECT MAX(datetime) AS '[timestamp]' FROM history_houry")
        dt_last_record_houry = DB.c.fetchone()[0]
        DB.c.execute("SELECT MAX(datetime) AS '[timestamp]' FROM history_daily")
        dt_last_record_daily = DB.c.fetchone()[0]

        if not dt_last_record_houry or dt_last_record_houry <= dt_last_record - timedelta(hours=1) :
            '''Construit les moyennes par heures si non enregistré pour les heures précédentes'''
            if dt_last_record_houry :
                DB.c.execute("SELECT datetime AS '[timestamp]', zone_id, temp, hrel, pressure, power FROM history WHERE datetime >= ? ORDER BY datetime ASC", [dt_last_record_houry+timedelta(hours=1)])
            else:
                DB.c.execute("SELECT datetime AS '[timestamp]', zone_id, temp, hrel, pressure, power FROM history ORDER BY datetime ASC",)
            dt_last_houry = dt_last_record_houry
            dts_houry = []
            zones_ids = []
            temps = {} #key: zone_id, value: liste pour la moyenne
            hrels = {}
            pressures = {}
            powers = {}
            for dt, zone_id, temp, hrel, pressure, power in DB.c.fetchall() :
                dt_houry = datetime(year=dt.year, month=dt.month, day=dt.day, hour=dt.hour)
                if dt_houry not in dts_houry and dt_houry < prev_dt_now_houry :
                    dts_houry.append(dt_houry)
                if not zone_id in zones_ids :
                    zones_ids.append(zone_id)
                    temps[zone_id] = []
                    hrels[zone_id] = []
                    pressures[zone_id] = []
                    powers[zone_id] = []
                if temp != None :
                    temps[zone_id].append((dt,temp))
                if hrel != None :
                    hrels[zone_id].append((dt,hrel))
                if pressure != None :
                    pressures[zone_id].append((dt,pressure))
                if power != None :
                    powers[zone_id].append((dt,power))

            # Moyenne par heure
            for dt_houry in dts_houry :
                logging.info('Calculs moyennes par heure pour %s',dt_houry.strftime('%Y-%m-%d %Hh'))
                next_dt_houry = dt_houry + timedelta(hours=1)
                next2_dt_houry = dt_houry + timedelta(hours=2)
                for zone_id in zones_ids :
                    h_entry = [dt_houry, zone_id]
                    for data in (temps[zone_id], hrels[zone_id], pressures[zone_id]) :
                        prev_dt = dt_houry
                        prev_datum = None
                        datadt_sum = 0.
                        dt_sum = 0.
                        average = None
                        for dt,datum in data :
                            if next2_dt_houry <= dt :
                                break
                            elif dt_houry < dt and prev_datum != None:
                                dt = min(dt, next_dt_houry)
                                datadt_sum += (dt - prev_dt).total_seconds() * (prev_datum + datum) / 2
                                dt_sum += (dt - prev_dt).total_seconds()
                                if next_dt_houry <= dt :
                                    break
                                prev_dt = dt
                            prev_datum = datum
                        if dt_sum:
                            average = datadt_sum/dt_sum
                        h_entry.append(average)

                    # on ne peut pas traiter "power" de la même façon
                    prev_dt = dt_houry
                    prev_datum = None
                    datadt_sum = 0.
                    average = 0.
                    for dt,datum in powers[zone_id] :
                        if next2_dt_houry <= dt :
                            break
                        elif dt_houry < dt and prev_datum != None:
                            dt = min(dt, next_dt_houry)
                            datadt_sum += (dt - prev_dt).total_seconds() * prev_datum
                            if next_dt_houry <= dt :
                                break
                            prev_dt = dt
                        prev_datum = datum
                    average = datadt_sum/timedelta(hours=1).total_seconds()
                    if average > 1. :
                        exit(1)
                    h_entry.append(average)

                    if h_entry[2] != None or h_entry[3] != None or h_entry[4] != None  :
                        self.houry.append(h_entry)

            #Data pour l'heure en cours
            for zone_id in zones_ids :
                self.houry_temps[zone_id] = [[dt_now_daily,None]]
                self.houry_hrels[zone_id] = [[dt_now_daily,None]]
                self.houry_pressures[zone_id] = [[dt_now_daily,None]]
                self.houry_powers[zone_id] = [[dt_now_daily,None]]
                for d in temps[zone_id]:
                    if d[0] >= dt_now_houry:
                        self.houry_temps[zone_id].append(d)
                    else :
                        self.houry_temps[zone_id] = [dt_now_daily,d[1]]
                for d in hrels[zone_id]:
                    if d[0] >= dt_now_houry:
                        self.houry_hrels[zone_id].append(d)
                    else :
                        self.daily_temps[zone_id] = [dt_now_daily,d[1]]
                for d in pressures[zone_id]:
                    if d[0] >= dt_now_houry:
                        self.daily_temps[zone_id].append(d)
                    else :
                        self.daily_temps[zone_id] = [dt_now_daily,d[1]]
                for d in powers[zone_id]:
                    if d[0] >= dt_now_houry:
                        self.houry_powers[zone_id].append(d)
                    else :
                        self.houry_powers[zone_id] = [dt_now_daily,d[1]]

        if not dt_last_record_daily or dt_last_record_houry <= dt_last_record - timedelta(hours=1) :
            '''Construit les moyennes par journées si non enregistrer pour les jours précédents'''
            if dt_last_record_daily :
                DB.c.execute("SELECT datetime AS '[timestamp]', zone_id, temp, hrel, pressure, power FROM history WHERE datetime >= ? ORDER BY datetime ASC", [dt_last_record_houry+timedelta(hours=1)])
            else:
                DB.c.execute("SELECT datetime AS '[timestamp]', zone_id, temp, hrel, pressure, power FROM history ORDER BY datetime ASC")
            dt_last_houry = dt_last_record_houry
            dts_daily = []
            zones_ids = []
            temps = {} #key: zone_id, value: liste pour la moyenne
            hrels = {}
            pressures = {}
            powers = {}
            for dt, zone_id, temp, hrel, pressure, power in DB.c.fetchall() :
                dt_daily = datetime(year=dt.year, month=dt.month, day=dt.day)
                if dt_daily not in dts_daily and dt_daily < prev_dt_now_daily :
                    dts_daily.append(dt_daily)
                if not zone_id in zones_ids :
                    zones_ids.append(zone_id)
                    temps[zone_id] = []
                    hrels[zone_id] = []
                    pressures[zone_id] = []
                    powers[zone_id] = []
                if temp != None :
                    temps[zone_id].append((dt,temp))
                if hrel != None :
                    hrels[zone_id].append((dt,hrel))
                if pressure != None :
                    pressures[zone_id].append((dt,pressure))
                if power != None :
                    powers[zone_id].append((dt,power))

            # Moyenne par jour
            for dt_daily in dts_daily :
                logging.info('Calculs moyennes par jour pour %s',dt_daily.strftime('%Y-%m-%d'))
                next_dt_daily = dt_daily + timedelta(days=1)
                next2_dt_daily = dt_daily + timedelta(days=2)
                for zone_id in zones_ids :
                    h_entry = [dt_daily, zone_id]
                    for data in (temps[zone_id], hrels[zone_id], pressures[zone_id]) :
                        prev_dt = dt_daily
                        prev_datum = None
                        datadt_sum = 0.
                        dt_sum = 0.
                        average = None
                        for dt,datum in data :
                            if next2_dt_daily <= dt :
                                break
                            elif dt_daily < dt and prev_datum != None:
                                dt = min(dt, next_dt_daily)
                                datadt_sum += (dt - prev_dt).total_seconds() * (prev_datum + datum) / 2
                                dt_sum += (dt - prev_dt).total_seconds()
                                if next_dt_daily <= dt :
                                    break
                                prev_dt = dt
                            prev_datum = datum
                        if dt_sum:
                            average = datadt_sum/dt_sum
                        h_entry.append(average)

                    # on ne peut pas traiter "power" de la même façon
                    prev_dt = dt_daily
                    prev_datum = None
                    datadt_sum = 0.
                    average = 0.
                    for dt,datum in powers[zone_id] :
                        if next2_dt_daily <= dt :
                            break
                        elif dt_daily < dt and prev_datum != None:
                            dt = min(dt, next_dt_daily)
                            datadt_sum += (dt - prev_dt).total_seconds() * prev_datum
                            if next_dt_daily <= dt :
                                break
                            prev_dt = dt
                        prev_datum = datum
                    average = datadt_sum/timedelta(days=1).total_seconds()
                    h_entry.append(average)

                    if h_entry[2] != None or h_entry[3] != None or h_entry[4] != None  :
                        self.daily.append(h_entry)

            #Data pour la journée en cours
            for zone_id in zones_ids :
                self.daily_temps[zone_id] = [[dt_now_daily,None]]
                self.daily_hrels[zone_id] = [[dt_now_daily,None]]
                self.daily_pressures[zone_id] = [[dt_now_daily,None]]
                self.daily_powers[zone_id] = [[dt_now_daily,None]]
                for d in temps[zone_id]:
                    if d[0] >= dt_now_daily:
                        self.daily_temps[zone_id].append(d)
                    else :
                        self.daily_temps[zone_id] = [dt_now_daily,d[1]]
                for d in hrels[zone_id]:
                    if d[0] >= dt_now_daily:
                        self.daily_hrels[zone_id].append(d)
                    else :
                        self.daily_hrels[zone_id] = [dt_now_daily,d[1]]
                for d in pressures[zone_id]:
                    if d[0] >= dt_now_daily:
                        self.daily_pressures[zone_id].append(d)
                    else :
                        self.daily_pressures[zone_id] = [dt_now_daily,d[1]]
                for d in powers[zone_id]:
                    if d[0] >= dt_now_daily:
                        self.daily_powers[zone_id].append(d)
                    else :
                        self.daily_powers[zone_id] = [dt_now_daily,d[1]]

        if Cfg.history_save_at :
            next_save_in = Cfg.history_save_at - timedelta(hours=now.hour,minutes=now.minute,seconds=now.second).total_seconds()
            if next_save_in < 0. :
                next_save_in += 24*60*60 #24h
            logging.debug("Prochaine sauvegarde de l'historique dans %.0f''"%next_save_in)
            timer = Timer(next_save_in, self._demon, ())
            timer.setDaemon(1)
            timer.start()


    def append(self,zone) :
        '''
        Insère une ligne dans l'historique
          object zone : la zone à insérer dans l'historique
        '''
        if Cfg.history_entry_every :
            '''Cfg.history_entry_every = 0 pas d'enregistrement de l'historique'''
            now = time()
            dt_now = datetime.now()
            dt_now_houry = datetime(year=dt_now.year, month=dt_now.month, day=dt_now.day, hour=dt_now.hour) #heure en cours
            dt_now_daily = datetime(year=dt_now.year, month=dt_now.month, day=dt_now.day) #journée en cours

            if zone.id not in self.zones_ids :
                self.zones_ids.append(zone.id)
                self.houry_temps[zone.id] = [[dt_now, None]]
                self.houry_hrels[zone.id] = [[dt_now, None]]
                self.houry_pressures[zone.id] = [[dt_now, None]]
                self.houry_powers[zone.id] = [[dt_now, None]]
                self.daily_temps[zone.id] = [[dt_now, None]]
                self.daily_hrels[zone.id] = [[dt_now, None]]
                self.daily_pressures[zone.id] = [[dt_now, None]]
                self.daily_powers[zone.id] = [[dt_now, None]]
                self.next_record[zone.id] = ceil(now/Cfg.history_entry_every)*Cfg.history_entry_every#arrondir au multiple "rond" supérieur
                super(History,self).append((datetime.now(),zone.id,zone.temp,zone.hrel,zone.pressure,zone.power))
                self.last_power[zone.id] = zone.power

            next_dt_houry = self.dt_houry+timedelta(hours=1)
            if dt_now >= next_dt_houry :
                '''Moyennes de l'heure'''
                for zone_id in self.zones_ids :
                    h_entry = [self.dt_houry, zone_id]
                    for data in (self.houry_temps[zone_id], self.houry_hrels[zone_id], self.houry_pressures[zone_id]) :
                        prev_dt = self.dt_houry
                        prev_datum = None
                        datadt_sum = 0.
                        dt_sum = 0.
                        average = None
                        for dt,datum in data :
                            if next_dt_houry <= dt < dt_now_houry and prev_datum != None:
                                datadt_sum += (next_dt_houry - prev_dt).total_seconds() * (prev_datum + datum) / 2
                                dt_sum += (next_dt_houry - prev_dt).total_seconds()
                                break
                            elif self.dt_houry < dt and prev_datum != None:
                                datadt_sum += (dt - prev_dt).total_seconds() * (prev_datum + datum) / 2
                                dt_sum += (dt - prev_dt).total_seconds()
                                prev_dt = dt
                            prev_datum = datum

                        if dt_sum :
                            average = datadt_sum/dt_sum
                        h_entry.append(average)

                    # on ne peut pas traiter "power" de la mme façon
                    prev_dt = self.dt_houry
                    prev_datum = None
                    datadt_sum = 0.
                    average = 0.
                    for dt,datum in self.houry_powers[zone_id] :
                        if next_dt_houry <= dt < dt_now_houry and prev_datum != None:
                            datadt_sum += (next_dt_houry - prev_dt).total_seconds() * prev_datum
                            break
                        elif self.dt_houry < dt and prev_datum != None:
                            datadt_sum += (dt - prev_dt).total_seconds() * prev_datum
                            prev_dt = dt
                        prev_datum = datum
                    average = datadt_sum/timedelta(hours=1).total_seconds()
                    h_entry.append(average)

                    if h_entry[2] != None or h_entry[3] != None or h_entry[4] != None  :
                        self.houry.append(h_entry)

                    #on conserve les dernières valeurs pour les moyennes suivantes
                    self.houry_temps[zone_id] = [[next_dt_houry,self.houry_temps[zone_id][-1][1]]]
                    self.houry_hrels[zone_id] = [[next_dt_houry,self.houry_hrels[zone_id][-1][1]]]
                    self.houry_pressures[zone_id] = [[next_dt_houry,self.houry_pressures[zone_id][-1][1]]]
                    self.houry_powers[zone_id] = [[next_dt_houry,self.houry_powers[zone_id][-1][1]]]

                self.dt_houry = self.dt_houry + timedelta(hours=1)

            next_dt_daily = self.dt_daily+timedelta(days=1)
            if dt_now >= next_dt_daily :
                '''Moyennes de la journée'''
                for zone_id in self.zones_ids :
                    h_entry = [self.dt_daily, zone_id]
                    for data in (self.daily_temps[zone_id], self.daily_hrels[zone_id], self.daily_pressures[zone_id]) :
                        prev_dt = self.dt_daily
                        prev_datum = None
                        datadt_sum = 0.
                        dt_sum = 0.
                        average = None
                        for dt,datum in data :
                            if next_dt_daily <= dt < dt_now_daily and prev_datum != None:
                                datadt_sum += (next_dt_daily - prev_dt).total_seconds() * (prev_datum + datum) / 2
                                dt_sum += (next_dt_daily - prev_dt).total_seconds()
                                break
                            elif self.dt_daily < dt and prev_datum != None:
                                datadt_sum += (dt - prev_dt).total_seconds() * (prev_datum + datum) / 2
                                dt_sum += (dt - prev_dt).total_seconds()
                                prev_dt = dt
                            prev_datum = datum
                        if dt_sum:
                            average = datadt_sum/dt_sum
                        h_entry.append(average)

                    # on ne peut pas traiter "power" de la même façon
                    prev_dt = self.dt_daily
                    prev_datum = None
                    datadt_sum = 0.
                    average = 0.
                    for dt,datum in self.daily_powers[zone_id] :
                        if next_dt_daily <= dt < dt_now_daily and prev_datum != None:
                            datadt_sum += (next_dt_daily - prev_dt).total_seconds() * prev_datum
                            break
                        elif self.dt_daily < dt and prev_datum != None:
                            datadt_sum += (dt - prev_dt).total_seconds() * prev_datum
                            prev_dt = dt
                        prev_datum = datum
                    average = datadt_sum/timedelta(hours=1).total_seconds()
                    h_entry.append(average)

                    if h_entry[2] != None or h_entry[3] != None or h_entry[4] != None  :
                        self.daily.append(h_entry)

                    #on conserve les dernières valeurs pour les moyennes suivantes
                    self.daily_temps[zone_id] = [[next_dt_daily,self.daily_temps[zone_id][-1][1]]]
                    self.daily_hrels[zone_id] = [[next_dt_daily,self.daily_hrels[zone_id][-1][1]]]
                    self.daily_pressures[zone_id] = [[next_dt_daily,self.daily_pressures[zone_id][-1][1]]]
                    self.daily_powers[zone_id] = [[next_dt_daily,self.daily_powers[zone_id][-1][1]]]

                self.dt_daily = self.dt_daily + timedelta(days=1)

                #on supprime les anciens enregistrements
                while self and self[0][0] < dt_now - timedelta(days=3) : # de plus de 3j pour l'instantané sans détruire l'objet
                    del (self[0])
                self.houry = [r for r in self.houry if r[0] > dt_now - timedelta(days=35)] # de plus de 35j pour les moyennes horaire
                self.daily = [r for r in self.daily if r[0] > dt_now - timedelta(days=370)] # de plus de 370j pour les moyennes journalières

            if now > self.next_record[zone.id] :
                logging.debug("Zone %i : temp, hrel, pressure"%zone.id)
                dt_next_record = datetime.fromtimestamp(self.next_record[zone.id])
                super(History,self).append((dt_next_record,zone.id,zone.temp,zone.hrel,zone.pressure,None))
                if zone.temp :
                    self.houry_temps[zone.id].append([dt_next_record,zone.temp])
                    self.daily_temps[zone.id].append([dt_next_record,zone.temp])
                if zone.hrel :
                    self.houry_hrels[zone.id].append([dt_next_record,zone.hrel])
                    self.daily_hrels[zone.id].append([dt_next_record,zone.hrel])
                if zone.pressure :
                    self.houry_pressures[zone.id].append([dt_next_record,zone.pressure])
                    self.daily_pressures[zone.id].append([dt_next_record,zone.pressure])
                self.next_record[zone.id] += Cfg.history_entry_every

            if zone.power != self.last_power[zone.id] :
                logging.debug("Zone %i : power"%zone.id)
                super(History,self).append((datetime.now(),zone.id,zone.temp,zone.hrel,zone.pressure,zone.power))
                self.last_power[zone.id] = zone.power
                self.houry_powers[zone.id].append([dt_now,zone.temp])
                self.daily_powers[zone.id].append([dt_now,zone.temp])


    def get(self,dt_from=None,dt_to=None,cols=None,zones=None,average=None) :
        '''
        Recupère l'historique
          datetime dt_from : à partir du ou None depuis toujours. None
                par défaut)
          datetime dt_to : jusqu'au ou None jusquèà maintenant. None par
                défaut)
          list cols : le valeur à récupéré ('temp','hrel,'pressure',
                'power') ou None pour tout récupéré. None par défaut.
          list zones : liste de id de zone à récupéré ou None pour
                toutes. None par défaut.
          str average : moyenne de données ('daily': par jours, 'houry':
                par heure, 'none': aucun, None : automatique),
                automatique par défaut (>60j: 'daily', >7j: 'houry',
                <=7j: 'none')
        Return : liste de tuples (datetimes, zone_id, cols**). Pas de
        zone_id si qu'une zone est demandéz, l'ordre des cols est celui
        de cols en entrée.
        '''

        if dt_from and dt_to :
            dt_delta = dt_to - dt_from
        elif dt_from :
            dt_delta = datetime.now() - dt_from
        elif dt_to :
            dt_delta = dt_to - self.dt_fist_record
        else :
            dt_delta = datetime.now() - self.dt_fist_record

        if average == 'daily' or (average != 'houry' and average != 'none' and dt_delta > timedelta(days=60)):
            table = 'history_daily'
            if dt_from :
                dt_from = dt_from-timedelta(days=1)
            if dt_to :
                dt_to = dt_to+timedelta(days=1)
            unsaved_history = self.daily
        elif average == 'houry' or (average != 'none' and dt_delta > timedelta(days=7)):
            table = 'history_houry'
            if dt_from :
                dt_from = dt_from-timedelta(hours=1)
            if dt_to :
                dt_to = dt_to+timedelta(hours=1)
            unsaved_history = self.houry
        else :
            table = 'history'
            if dt_from :
                dt_from = dt_from-timedelta(seconds=Cfg.history_entry_every)
            if dt_to :
                dt_to = dt_to+timedelta(seconds=Cfg.history_entry_every)
            unsaved_history = self

        where = []
        values = []

        if dt_from :
            where.append('datetime > ?')
            values.append(dt_from)

        if dt_to :
            where.append('datetime < ?')
            values.append(dt_to)

        select = ["datetime AS '[timestamp]'"]
        if not cols :
            cols = ['temp','hrel','pressure','power']
        for col in cols :
            if col in ['temp','hrel','pressure','power'] :
                select.append(col)

        if zones :
            zone_in = []
            for i in range(len(zones)) :
                zones[i]=int(zones[i])
                zone_in.append('?')
                values.append(zones[i])
            where.append('zone_id IN (%s)'%','.join(zone_in))

        if not zones or len(zones) > 1 :
            select.insert(1,'zone_id')

        if where :
            DB.c.execute("SELECT %s FROM %s WHERE %s"%(','.join(select), table, ' AND '.join(where)),values)
        else :
            DB.c.execute("SELECT %s FROM %s"%(','.join(select), table))

        history = DB.c.fetchall()
        for entry in unsaved_history :
            if  (not dt_from or entry[0] > dt_from) and (not dt_to or entry[0] < dt_to) and (not zones or entry[1] in zones) :
                if select :
                    e = (entry[0],) #date
                    if not zones or len(zones) > 1 :
                        e+=(entry[1],)
                    if 'temp' in select :
                        e+=(entry[2],)
                    if 'hrel' in select :
                        e+=(entry[3],)
                    if 'pressure' in select :
                        e+=(entry[4],)
                    if 'power' in select :
                        e+=(entry[5],)
                    history.append(e)
                else : #select = *
                    history.append(entry)

        history.sort()

        return history


    def _demon(self) :
        '''
        Afin d'éviter de multiplier les écitures, sauvegarde 1 fois par
        jour
        '''
        self._save()
        now = datetime.now()
        next_save_in = Cfg.history_save_at - timedelta(hours=now.hour,minutes=now.minute,seconds=now.second).total_seconds() + 24*60*60
        logging.debug("Prochaine sauvegarde de l'historique dans %.0f''"%next_save_in)
        timer = Timer(next_save_in, self._demon, ())
        timer.setDaemon(1)
        timer.start()


    def _save(self) :
        '''
        Sauvegarde dans la base de donnée de l'historique
        '''
        from database import DB as DB_save
        DB_save = DB_save()

        logging.info('Sauvegarde de l\'historique')

        DB_save.c.execute("SELECT MAX(datetime) AS '[timestamp]' FROM history")
        dt_last_record = DB_save.c.fetchone()[0]
        DB_save.c.executemany('INSERT INTO history (datetime,zone_id,temp,hrel,pressure,power) VALUES (?,?,?,?,?,?)',[r for r in self if r[0] > dt_last_record])

        DB_save.c.execute("SELECT MAX(datetime) AS '[timestamp]' FROM history_houry")
        dt_last_record_houry = DB_save.c.fetchone()[0]
        DB_save.c.executemany('INSERT INTO history_houry (datetime,zone_id,temp,hrel,pressure,power) VALUES (?,?,?,?,?,?)',[r for r in self.houry if r[0] > dt_last_record_houry])

        DB_save.c.execute("SELECT MAX(datetime) AS '[timestamp]' FROM history_daily")
        dt_last_record_daily = DB_save.c.fetchone()[0]
        DB_save.c.executemany('INSERT INTO history_daily (datetime,zone_id,temp,hrel,pressure,power) VALUES (?,?,?,?,?,?)',[r for r in self.daily if r[0] > dt_last_record_daily])

        DB_save.conn.commit()
        DB_save.conn.close()

