#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import subprocess, os, sys
from datetime import datetime, time, date, timedelta
from pkg_resources import parse_version
import version as Version


from database import DB
DB = DB()

old_db_struct = DB.c.execute("SELECT value FROM config WHERE key = 'db_struct' ").fetchone()[0]
old_version = DB.c.execute("SELECT value FROM config WHERE key = 'version' ").fetchone()[0]

if parse_version(old_db_struct) != parse_version(Version.db_struct) :
    print "Upgrade db_structure %s"%old_db_struct

    if parse_version(old_db_struct) < parse_version('1.1.0') :
        print "--> to 1.1.0"
        DB.c.executescript("""
            DROP TABLE `schedules_mode`;
            CREATE TABLE `modes` (
                `id`    INTEGER UNIQUE,
                `name`  TEXT,
                PRIMARY KEY(id)
            );
            INSERT INTO config VALUES ('mode_id',1);
            INSERT INTO modes (name) VALUES ('Default');
            UPDATE config SET value = '1.1.0' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

    if parse_version(old_db_struct) < parse_version('1.1.1') :
        print "--> to 1.1.1"
        DB.c.executescript("""
            UPDATE config SET key = 'away_from' WHERE key = 'holiday_from';
            UPDATE config SET key = 'away_to' WHERE key = 'holiday_to';
            ALTER TABLE zones RENAME TO tmp_zones;
            CREATE TABLE zones (
                `id`    INTEGER UNIQUE,
                `name`  TEXT,
                `away_temp`  NUMERIC DEFAULT 12,
                PRIMARY KEY(id)
            );
            INSERT INTO zones(id,name,away_temp) SELECT id,name,holiday_temp FROM tmp_zones;
            DROP TABLE tmp_zones;
            UPDATE config SET value = '1.1.1' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

    if parse_version(old_db_struct) < parse_version('1.1.2') :
        print "--> to 1.1.2"
        DB.c.executescript("""
            INSERT INTO `config` VALUES ('default_temp',6.0);
            UPDATE config SET value = '1.1.2' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

    if parse_version(old_db_struct) < parse_version('1.2.0') :
        print "--> to 1.2.0"
        DB.c.executescript("""
            DROP TABLE zones_log;
            DROP TABLE sensors_log;
            DROP TABLE pilots_log;
            CREATE TABLE `logs` (
                `datetime` INTEGER,
                `zone_id`   INTEGER,
                `temp`  NUMERIC DEFAULT NULL,
                `hrel`  NUMERIC DEFAULT NULL,
                `pressure`  NUMERIC DEFAULT NULL,
                `power`  INTEGER DEFAULT NULL
            );
            INSERT INTO `config` VALUES ('log_every',15*60);
            INSERT INTO `config` VALUES ('save_log_at',4*60*60);
            UPDATE config SET value = '1.2.0' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

    if parse_version(old_db_struct) < parse_version('1.3.0') :
        print "--> to 1.3.0"
        DB.c.executescript("""
            ALTER TABLE zones RENAME TO tmp_zones;
            CREATE TABLE zones (
                `id`    INTEGER UNIQUE,
                `name`  TEXT,
                `away_temp`  NUMERIC DEFAULT 12,
                `speed_temp_climb` NUMERIC DEFAULT 0,
                PRIMARY KEY(id)
            );
            INSERT INTO zones(id,name,away_temp) SELECT id,name,away_temp FROM tmp_zones;
            DROP TABLE tmp_zones;
            UPDATE config SET value = '1.3.0' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

    if parse_version(old_db_struct) < parse_version('1.3.1') :
        print "--> to 1.3.1"
        DB.c.executescript("""
            ALTER TABLE logs RENAME TO history;
            UPDATE config SET key = 'history_entry_every' WHERE key = 'log_every';
            UPDATE config SET key = 'history_save_at' WHERE key = 'save_log_at';
            UPDATE config SET value = '1.3.1' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

    if parse_version(old_db_struct) < parse_version('1.4.0') :
        print "--> to 1.4.0"
        DB.c.executescript("""
            CREATE TABLE `history_houry` (
                `datetime` INTEGER,
                `zone_id`   INTEGER,
                `temp`  NUMERIC DEFAULT NULL,
                `hrel`  NUMERIC DEFAULT NULL,
                `pressure`  NUMERIC DEFAULT NULL,
                `power`  NUMERIC DEFAULT NULL
            );
            CREATE TABLE `history_daily` (
                `datetime` INTEGER,
                `zone_id`   INTEGER,
                `temp`  NUMERIC DEFAULT NULL,
                `hrel`  NUMERIC DEFAULT NULL,
                `pressure`  NUMERIC DEFAULT NULL,
                `power`  NUMERIC DEFAULT NULL
            );
            UPDATE config SET value = '1.4.0' WHERE key = 'db_struct';
        """)
        DB.conn.commit()

else :
    print "Not upgrade require for db_structure"


if parse_version(old_version) != parse_version(Version.version) :
    print "upgrade program to %s"%Version.version
    DB.c.execute("UPDATE config SET value = ? WHERE key = 'version' ",[ Version.version ])
    DB.conn.commit()
    print "Program upgrade finished"
else :
    print "Not upgrade require for program"
