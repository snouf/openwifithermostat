#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# Doc
#   - http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask

import os
from flask import Flask, jsonify, abort, request, render_template, render_template_string, url_for, send_from_directory, send_file, redirect, Response, stream_with_context
from json import dumps
import requests

app = Flask(__name__)
title = __name__

if __name__ == '__main__':
    import __init__

import Cfg, UdpDevices, Zones, Sensors, Pilots, Modes, History
from datetime import datetime, timedelta
from time import mktime
import logging
logging.basicConfig(level=logging.DEBUG)

def timestamp(dt=None):
    if dt == None :
        dt=datetime.now()
    return mktime(dt.timetuple())+float(dt.microsecond)/1000000


def check_data(data,crumbskeys_checks) :
    '''
    Verifie les donnée, si le type est incorect tente de le convertir
      data: données à varifié (générallement en dict)
      crumbskeys_checks: une liste de tuple (crumbskeys,checks)
          str|list crumbskeys: la clef à vérifié (ex: 'name' ou le
                chemin vers la clef (ex: ['temp','now']). '*' peut être
                utilisé pour itérer tout les clefs (d'une liste ou d'un
                dico).
          dict checks: dico des vérifications. 'type' le type, 'require'
                champs requis (False par défaut), 'defaut': valeur par
                défaut (None par defaut).
      Exemple 1 : ('location',{'type':str,'require':True})
      Exemple 2 : ('weight',{'type':float,'default':1.0})
    Return: (data,errors) data: les données éventuellement corrigées,
            errors, la liste des éventuelles erreurs.
    '''

    def check(data,crumbkeys) :
        type_data = type(data)
        if len(crumbkeys) == 0 :
            if 'require' in checks and checks['require'] and data == None :
                '''Valeur requise retourne une erreur si absente'''
                return None,['Value for "%s" require'%"→".join(crumbskey)]
            if 'default' in checks and not data :
                '''Valeur par défaut retourne cette valeur si le champe est vide'''
                return checks['default'],[]
            if checks.has_key('type') and data != None:
                '''type spécifié, tente de convertire si échoue retourne une erreur'''
                try :
                    return checks['type'](data),[]
                except :
                    return None,['Value for "%s" must be a %s'%("→".join(crumbskey),checks['type'])]
            else :
                '''Pas de type spécifié retourne la valeur telsquel'''
                return data,[]
        elif crumbkeys[0] == '*' and type_data == list :
            '''on iter une liste'''
            new_data, errors = []
            for datum in data :
                new_datum,error = check(datum,crumbkeys[1:])
                new_data.append(new_datum)
                errors += error
            return new_data,errors
        elif crumbkeys[0] == '*' and type_data == dict :
            '''on iter un dict'''
            for key,datum in data.items() :
                data[key],error = check(datum,crumbkeys[1:])
                errors += error
            return new_data,errors
        elif type_data == dict and crumbkeys[0] in data :
            data[crumbkeys[0]],errors = check(data[crumbkeys[0]],crumbkeys[1:])
            return data,errors
        elif (type_data == dict and crumbkeys[0] not in data) or (type_data == None) :
            if 'require' in checks and checks['require'] :
                '''Valeur requise retourne une erreur si absente'''
                return data,['Value for "%s" require'%("→".join(crumbskey))]
            else :
                '''Valeur par défaut ou none (par défaut de valeur par défaut)'''
                data[crumbkeys[0]],errors = check(None,crumbkeys[1:])
                return data,errors
        else :
            data = {}
            data[crumbkeys[0]],errors = check(None,crumbkeys[1:])
            return data,errors


    errors = []
    for crumbskey, checks in crumbskeys_checks :
        if type(crumbskey) == str :
            crumbskey = [crumbskey]
        data,error = check(data,crumbskey)
        errors += error

    return data,errors


##UI


#js/css

@app.route('/js/<path:filename>')
def send_js(filename):
    return send_from_directory('js', filename)


@app.route('/extensions/<path:filename>')
def send_extensions(filename):
    if filename == 'index.js' :
        available_extensions = []
        for path in os.listdir('extensions'):
            if os.path.isfile(os.path.join('extensions',path,'meta.js')) :
                available_extensions.append(path)
        return jsonify({'available_extensions':available_extensions})
    elif not '..' in filename :
        if filename.endswith('.html') :
            with open('extensions/'+filename) as fp :
                return render_template_string(fp.read().decode('utf-8'))
        else :
            return send_file('extensions/'+filename)
    else :
        abort(404)


@app.route('/css/<path:filename>')
def send_css(filename):
    return send_from_directory('css', filename)


#template


@app.route('/', methods=['GET'])
def ui_index():
    return render_template('welcome.html')


@app.route('/modes', methods=['GET'])
def ui_modes():
    return render_template('modes.html', modes=Modes.get_all())


@app.route('/modes/add', methods=['GET'])
def ui_mode_add():
    return render_template('mode_add.html')


@app.route('/modes/<int:mode_id>/edit', methods=['GET'])
def ui_mode_edit(mode_id):
    mode = Modes.get(mode_id)
    return render_template('mode_edit.html', mode=mode)


@app.route('/zones/<int:zone_id>', methods=['GET'])
def ui_zone(zone_id):
    zone = Zones.get(zone_id);
    return render_template('zone.html', zone=zone)


@app.route('/zones/add', methods=['GET'])
def ui_zone_add():
    return render_template('zone_add.html')


@app.route('/zones/<int:zone_id>/edit', methods=['GET'])
def ui_zone_params(zone_id):
    zone = Zones.get(zone_id);
    return render_template('zone_edit.html', zone=zone)


@app.route('/zones/<int:zone_id>/schedules/weekly', methods=['GET'])
def ui_zone_schedules_weekly(zone_id):
    zone = Zones.get(zone_id);
    return render_template('schedules_weekly.html', zone=zone)


@app.route('/zones/<int:zone_id>/schedules/<int:week_second>', methods=['GET'])
def ui_zone_schedules_edit(zone_id,week_second):
    zone = Zones.get(zone_id);
    return render_template('schedules_edit.html', zone_id=zone_id, zone_name=zone.name, week_second=week_second)


@app.route('/zones/<int:zone_id>/schedules/add', methods=['GET'])
def ui_zone_schedules_add(zone_id):
    zone = Zones.get(zone_id);
    return render_template('schedules_edit.html', zone_id=zone_id, zone_name=zone.name, week_second=-1)


@app.route('/zones/<int:zone_id>/sensors/<int:sensor_id>/edit', methods=['GET'])
def ui_zone_sensor_edit(zone_id,sensor_id):
    zone = Zones.get(zone_id);
    sensor = Sensors.get(sensor_id);
    return render_template('zone_sensor_edit.html', zone=zone, sensor=sensor)


@app.route('/zones/<int:zone_id>/pilots/<int:pilot_id>/edit', methods=['GET'])
def ui_zone_pilot_edit(zone_id,pilot_id):
    zone = Zones.get(zone_id);
    pilot = Pilots.get(pilot_id);
    return render_template('zone_pilot_edit.html', zone=zone, pilot=pilot)


@app.route('/away', methods=['GET'])
def ui_away():
    return render_template('away.html')


@app.route('/sensors', methods=['GET'])
def ui_sensors():
    return render_template('sensors.html')


@app.route('/sensors/<int:sensor_id>/edit', methods=['GET'])
def ui_sensor_edit(sensor_id):
    sensor = Sensors.get(sensor_id);
    return render_template('sensor_edit.html',sensor=sensor)


@app.route('/pilots', methods=['GET'])
def ui_pilots():
    return render_template('pilots.html')


@app.route('/pilots/<int:pilot_id>/edit', methods=['GET'])
def ui_pilot_edit(pilot_id):
    pilot = Pilots.get(pilot_id);
    return render_template('pilot_edit.html',pilot=pilot)


@app.route('/history', methods=['GET'])
def ui_history():
    return render_template('history.html')


@app.route('/params', methods=['GET'])
def ui_others_settings():
    return render_template('params.html')


@app.route('/extensions', methods=['GET'])
def ui_extensions():
    return render_template('extensions.html')


## API


# datetime

@app.route('/api/v1.0/datetime', methods=['GET'])
def api_datetime_GET():
    '''
    GET : retourne la date (timestamp)
    '''
    return jsonify({'datetime': datetime.now()})


# modes

@app.route('/api/v1.0/modes', methods=['GET'])
def api_modes_GET():
    '''
    GET : retourne les modes
    '''
    modes = []
    for mode in Modes.get_all() :
        modes.append({
            'id':mode.id,
            'name':mode.name,
        })
    return jsonify({'modes': modes})


@app.route('/api/v1.0/modes', methods=['POST'])
def api_modes_POST():
    '''
    POST {name=STR} : ajout un mode ("unnamed" par defaut)
    '''
    data,errors = check_data(request.get_json(force=True),[
        ('name', {'type':str,'default':'unnamed'})
    ])
    if errors :
        return jsonify({'errors':errors})
    mode = Modes.add(data['name'])
    return jsonify({
        'id':mode.id,
        'name':mode.name,
    })


@app.route('/api/v1.0/modes/<int:mode_id>', methods=['GET','PUT'])
def api_mode_GET_PUT(mode_id):
    '''
    <int:mode_id> : l'id du mode
    GET : le mode
    PUT {name:STR} : modifie le nom du mode
    '''
    mode = Modes.get(mode_id)
    if request.method == 'PUT' :
        data,errors = check_data(request.get_json(force=True),[
            ('name',{'type':str})
        ])
        if errors :
            return jsonify({'errors':errors})
        if data['name']:
            mode.set_name(data['name'])
    return jsonify({
        'id':mode.id,
        'name':mode.name,
    })


@app.route('/api/v1.0/modes/<int:mode_id>', methods=['DELETE'])
def api_mode_DELETE(mode_id):
    '''
    <int:mode_id> : l'id du mode
    DELETE : supprime le mode
    '''
    Modes.delete(mode_id)
    return api_modes_GET()


@app.route('/api/v1.0/modes/active', methods=['GET','PUT'])
def api_mode_active_GET_PUT():
    '''
    GET : le mode actif
    PUT {id:INT} : définit le mode actif
    PUT {name:STR} : modifie le nom du mode actif
    '''
    if request.method == 'PUT' :
        data,errors = check_data(request.get_json(force=True),[
            ('id',{'type':int}),
            ('name',{'type':str}),
        ])
        if errors :
            return jsonify({'errors':errors})
        if data['id']:
            Modes.set_active(data['id'])
        if data['name']:
            Modes.active.set_name(data['name'])
    return jsonify({
        'id':Modes.active.id,
        'name':Modes.active.name,
    })


# zones

@app.route('/api/v1.0/zones', methods=['GET'])
def api_zones_GET():
    '''
    GET : liste des zones
    '''
    zones = []
    for zone in Zones.get_all() :
        zones.append({
            'id':zone.id,
            'name':zone.name,
            'temp':zone.temp,
            'hrel':zone.hrel,
            'pressure':zone.pressure,
            'away_temp':zone.away_temp,
            'speed_temp_climb':zone.speed_temp_climb
        })

    return jsonify({'zones': zones})


@app.route('/api/v1.0/zones', methods=['POST'])
def api_zones_POST():
    '''
    POST {name=STR} : ajout une zone (unnamed par défaut)
    '''
    data,errors = check_data(request.get_json(force=True),[
        ('name',{'type':str,'default':'unnamed'}),
        ('away_temp',{'type':float,'default':6.}),
        ('speed_temp_climb',{'type':float,'default':0.})
    ])
    if errors :
        return jsonify({'errors':errors})
    zone = Zones.add(data['name'],data['away_temp'],data['speed_temp_climb'])
    return jsonify({
        'id':zone.id,
        'name':zone.name,
        'temp':zone.temp,
        'hrel':zone.hrel,
        'pressure':zone.pressure,
        'away_temp':zone.away_temp,
        'speed_temp_climb':zone.away_temp
    })


@app.route('/api/v1.0/zones/<int:zone_id>', methods=['GET','PUT'])
def api_zone_GET(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    GET : la zone
    PUT {name:STR} : modifie le nom de la zone
    '''
    zone = Zones.get(zone_id)
    if request.method == 'PUT' :
        data,errors = check_data(request.get_json(force=True),[
            ('name',{'type':str}),
            ('away_temp',{'type':float,'default':6.}),
            ('speed_temp_climb',{'type':float,'default':0.})
        ])
        if errors :
            return jsonify({'errors':errors})
        if data['name']:
            zone.set_name(data['name'])
        if data['away_temp']:
            zone.set_away_temp(data['away_temp'])
        if data['speed_temp_climb']:
            zone.set_speed_temp_climb(data['speed_temp_climb'])
    return jsonify({
        'id':zone.id,
        'name':zone.name,
        'temp':zone.temp,
        'hrel':zone.hrel,
        'pressure':zone.pressure,
        'away_temp':zone.away_temp,
        'speed_temp_climb':zone.speed_temp_climb
    })


@app.route('/api/v1.0/zones/<int:zone_id>', methods=['DELETE'])
def api_zone_DELETE(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    DELETE : supprime la zone
    '''
    Zones.remove(zone_id)
    return api_zones_GET()


@app.route('/api/v1.0/zones/<int:zone_id>/schedules/now', methods=['GET', 'PUT'])
def api_zone_schedule_now_GET_PUT(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    GET : la date (timestamp), la plage de programmation en cours (
            début, fin en timestamp), les températures (voulue
            actuellement, prochaine en °C)
    PUT {time:{end:TIMESTAMP}} : modifie provisoirement la fin de la
            plage de programmation actuelle
    PUT {temp:{now:FLOAT} : modifie provisoirement la température de la
            plage de programmation actuelle
    '''
    zone = Zones.get(zone_id)
    if request.method == 'PUT' :
        data,errors = check_data(request.get_json(force=True),[
            (['temp','now'],{'type':float}),
            (['time','end'],{'type':float})
        ])
        if errors :
            return jsonify({'errors':errors})
        if data['temp']['now'] :
            zone.set_cur_temp(data['temp']['now'])
        if data['time']['end'] :
            zone.set_end_time(datetime.fromtimestamp(data['time']['end']))
    normal_start_dt, normal_cur_temp = zone.normal_cur_schedule
    start_dt, cur_temp = zone.cur_schedule
    normal_end_dt, normal_next_temp = zone.next_schedule
    end_dt, next_temp = zone.next_schedule
    return jsonify({
        'now':timestamp(),
        'time':{'start':timestamp(start_dt),'end':timestamp(end_dt),'normal_end':timestamp(normal_end_dt)},
        'temp':{'now':cur_temp,'normal_now':normal_cur_temp,'next':next_temp}
    })


@app.route('/api/v1.0/zones/<int:zone_id>/schedules/weekly', methods=['GET','POST'])
def api_zone_schedules_weekly_GET_POST(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    GET : la programation hebdomadaire sous la forme d'une liste de
            plage de programation [week_second, temp]. week_second:
            secondes depuis le début de la semaine (lundi 00:00:00),
            temp: température en °C. week_second sera arrondi au 1/4
            d'heure.
    POST {week_second:FLOAT,temp:FLOAT}: ajoute la plage (si week_second
            existe remplace l'ancienne plage)
    '''
    zone = Zones.get(zone_id)
    if request.method == 'POST' :
        data,errors = check_data(request.get_json(force=True),[
            ('week_second',{'type':float,'require':True}),
            ('temp',{'type':float,'require':True})
        ])
        if errors :
            return jsonify({'errors':errors})
        week_second, temp = zone.Schedules.add_one(data['week_second'],data['temp'])
        zone.reload(reload_name=False)
        return jsonify({
            'now':timestamp(),
            'week_second':week_second,
            'temp':temp
        })
    else :
        data = {
            'now':timestamp(),
            'schedules': []
        }
        for (week_second, temp)  in zone.Schedules.weekly() :
            data['schedules'].append({
                'week_second':week_second,
                'temp':temp
            })
        return jsonify(data)


@app.route('/api/v1.0/zones/<int:zone_id>/schedules/weekly/<int:week_seconds>', methods=['PUT','DELETE'])
def api_zone_schedules_weekly_PUT_DELETE(zone_id,week_seconds):
    '''
    <int:zone_id> : l'id de la zone
    <int:week_seconds> : second de la semaines
    PUT {week_second:FLOAT,temp:FLOAT} : modifie la plage de
            programmation
    DELETE : supprime la plage de programmation
    '''
    zone = Zones.get(zone_id)
    if request.method == 'PUT' :
        data,errors = check_data(request.get_json(force=True),[
            ('week_second',{'type':float,'require':True}),
            ('temp',{'type':float,'require':True})
        ])
        if errors :
            return jsonify({'errors':errors})
        week_seconds = zone.Schedules.remove_one(week_seconds)
        week_second, temp = zone.Schedules.add_one(data['week_second'],data['temp'])
        zone.reload(reload_name=False)
        return jsonify({
            'now':timestamp(),
            'week_second':week_second,
            'temp':temp
        })
    elif request.method == 'DELETE':
        week_seconds = zone.Schedules.remove_one(week_seconds)
        zone.reload(reload_name=False)
        if week_seconds != None :
            return jsonify({
                'now':timestamp(),
                'success':True
            })
        else :
            return jsonify({
                'now':timestamp(),
                'warnings':['last schedule can\'t be remove']
            })


@app.route('/api/v1.0/zones/<int:zone_id>/sensors', methods=['GET'])
def api_zone_sensors_GET(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    GET : les capteurs de cette zone
    '''
    zone = Zones.get(zone_id)
    data = {
        'now':timestamp(),
        'id':zone.id,
        'name':zone.name,
        'temp':zone.temp,
        'hrel':zone.hrel,
        'pressure':zone.pressure,
        'sensors':[]
    }
    for sensor in zone.sensors :
        data['sensors'].append({
            'id':sensor.id,
            'name':sensor.name,
            'location':sensor.location,
            'temp':sensor.temp,
            'hrel':sensor.hrel,
            'pressure':sensor.pressure,
            'weight':sensor.weights[zone.id]
        })
    return jsonify(data)


@app.route('/api/v1.0/zones/<int:zone_id>/sensors', methods=['POST'])
def api_zone_sensors_POST(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    POST {location:STR} : ajoute le capteur (location REQUIS adresse du
            capteur `openwifithermostat.sensor...`)
    POST {name} : définit le nom du capteur (si non spécifié ou vide
            recupéré sur le capteur)
    POST {weight} : définit la pondération pour la moyenne (par défaut 1)
    '''
    zone = Zones.get(zone_id)
    data,errors = check_data(request.get_json(force=True),[
        ('location',{'type':str,'require':True}),
        ('name',{'type':str}),
        ('weight',{'type':float,'default':1.0})
    ])
    if errors :
        return jsonify({'errors':errors})
    if not data['name'] :
        data['name'] = UdpDevices.get(data['location']).get_datum('name')
    sensor = zone.add_sensor(name=data['name'],location=data['location'],weight=data['weight'])
    return jsonify({
        'now':timestamp(),
        'id':sensor.id,
        'name':sensor.name,
        'location':sensor.location,
        'temp':sensor.location,
        'hrel':sensor.hrel,
        'pressure':sensor.pressure,
        'weight':sensor.weights[zone.id]
    })


@app.route('/api/v1.0/zones/<int:zone_id>/sensors/<int:sensor_id>', methods=['PUT'])
def api_zone_sensors_PUT(zone_id,sensor_id):
    '''
    <int:zone_id> : l'id de la zone
    <int:sensor_id> : l'id du capteur
    PUT {name} : modifie nom du capteur (si non spécifié ou vide
            récupérer sur le capteur)
    PUT {weight} : modifie la pondération
    '''
    zone = Zones.get(zone_id)
    data,errors = check_data(request.get_json(force=True),[
        ('name',{'type':str}),
        ('weight',{'type':float})
    ])
    if errors :
        return jsonify({'errors':errors})
    sensor = Sensors.get(sensor_id)
    if data['name'] :
        sensor.update(name=data['name'])
    if data['weight'] :
        sensor.set_weight(zone_id=zone.id, weight=data['weight'])
    return jsonify({
        'now':timestamp(),
        'id':sensor.id,
        'name':sensor.name,
        'location':sensor.location,
        'temp':sensor.location,
        'hrel':sensor.hrel,
        'pressure':sensor.pressure,
        'weight':sensor.weights[zone.id]
    })


@app.route('/api/v1.0/zones/<int:zone_id>/sensors/<int:sensor_id>', methods=['DELETE'])
def api_zone_sensors_DELETE(zone_id,sensor_id):
    '''
    <int:zone_id> : l'id de la zone
    <int:sensor_id> : l'id du capteur
    DELETE : supprime le capteur pour cette zone
    '''
    Zones.get(zone_id).remove_sensor(sensor_id)
    return jsonify({
        'now':timestamp(),
        'success':True
    })


@app.route('/api/v1.0/zones/<int:zone_id>/pilots', methods=['GET'])
def api_zone_pilots_GET(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    GET : les pilotes de cette zone
    '''
    zone = Zones.get(zone_id)
    data = {
        'now':timestamp(),
        'pilots': []
    }
    for pilot in zone.pilots :
        data['pilots'].append({
            'id':pilot.id,
            'name':pilot.name,
            'location':pilot.location,
            'logic_reverse':pilot.logic_reverse,
            'power':pilot.power
        })
    return jsonify(data)


@app.route('/api/v1.0/zones/<int:zone_id>/pilots', methods=['POST'])
def api_zone_pilots_POST(zone_id):
    '''
    <int:zone_id> : l'id de la zone
    POST {location:STR} : ajoute le pilote (location REQUIS adresse du
            pilote `openwifithermostat.pilot...`)
    POST {name} : définit nom du pilot (si non spécifié ou vide
            recupérer sur le pilote)
    POST {logic_reverse} : définit l'inversion de logique (sur marche si
            la température est atteinte) (False par defaut)
    '''
    zone = Zones.get(zone_id)
    data,errors = check_data(request.get_json(force=True),[
        ('location',{'type':str,'require':True}),
        ('name',{'type':str}),
        ('logic_reverse',{'type':bool,'default':False})
    ])
    if errors :
        return jsonify({'errors':errors})
    if not data['name'] :
        data['name'] = UdpDevices.get(data['location']).get_datum('name')
    pilot = zone.add_pilot(name=data['name'],location=data['location'],logic_reverse=data['logic_reverse'])
    return jsonify({
        'now':timestamp(),
        'id':pilot.id,
        'name':pilot.name,
        'location':pilot.location,
        'logic_reverse':pilot.logic_reverse,
        'power':pilot.power
    })


@app.route('/api/v1.0/zones/<int:zone_id>/pilots/<int:pilot_id>', methods=['PUT'])
def api_zone_pilots_PUT(zone_id,pilot_id):
    '''
    <int:zone_id> : l'id de la zone
    <int:pilot_id> : l'id du pilote
    PUT {name} : modifie le nom du pilote (si non spécifié ou vide
            récupérer sur le pilots)
    PUT {logic_reverse} : modifie l'inversion de logique
    '''
    zone = Zones.get(zone_id)
    data,errors = check_data(request.get_json(force=True),[
        ('name',{'type':str}),
        ('logic_reverse',{'type':bool})
    ])
    if errors :
        return jsonify({'errors':errors})
    pilot = zone.pilots.get(pilot_id)
    if data['name'] :
        pilot.set_name(data['name'])
    if data['logic_reverse'] :
        pilot.set_name(data['logic_reverse'])
    return jsonify({
        'now':timestamp(),
        'id':pilot.id,
        'name':pilot.name,
        'location':pilot.location,
        'logic_reverse':pilot.logic_reverse,
        'power':pilot.power
    })


@app.route('/api/v1.0/zones/<int:zone_id>/pilots/<int:pilot_id>', methods=['DELETE'])
def api_zone_pilots_DELETE(zone_id,pilot_id):
    '''
    <int:zone_id> : l'id de la zone
    <int:pilot_id> : l'id du pilote
    DELETE : supprime le pilote pour cette zone
    '''
    Zones.get(zone_id).remove_pilot(pilot_id)
    return jsonify({
        'now':timestamp(),
        'success':True
    })


#sensors

@app.route('/api/v1.0/sensors', methods=['GET'])
def api_sensors_GET():
    '''
    GET : les capteurs toutes zones confondues
    '''
    data = {
        'now':timestamp(),
        'sensors': []
    }
    for sensor in Sensors :
        data['sensors'].append({
            'id':sensor.id,
            'name':sensor.name,
            'location':sensor.location,
            'temp':sensor.temp,
            'hrel':sensor.hrel,
            'pressure':sensor.pressure,
            'weights':sensor.weights
        })
    return jsonify(data)


@app.route('/api/v1.0/sensors', methods=['POST'])
def api_sensors_POST():
    '''
    POST {location:STR} : ajoute le capteur (location REQUIS adresse du
            capteur `openwifithermostat.sensor...`).
    POST {name:STR} : définit nom du capteur. Si vide ou absent,
            recupéré sur le capteur
    '''
    #TODO à tester
    data,errors = check_data(request.get_json(force=True),[
        ('location',{'type':str,'require':True}),
        ('name',{'type':str})
    ])
    if errors :
        return jsonify({'errors':errors})
    if not data['name'] :
        data['name'] = UdpDevices.get(data['location']).get_datum('name')
    sensor = Sensors.add(name=data['name'],location=data['location'])
    return jsonify({
        'now':timestamp(),
        'id':sensor.id,
        'name':sensor.name,
        'location':sensor.location,
        'temp':sensor.location,
        'hrel':sensor.hrel,
        'pressure':sensor.pressure,
    })


@app.route('/api/v1.0/sensors/<int:sensor_id>', methods=['PUT'])
def api_sensors_PUT(sensor_id):
    '''
    <int:sensor_id> : l'id du capteur
    PUT {name} : modifie le nom du capteur
    '''
    data,errors = check_data(request.get_json(force=True),[
        ('name',{'type':str}),
    ])
    if errors :
        return jsonify({'errors':errors})
    sensor = Sensors.get(sensor_id)
    if data['name'] :
        sensor.set_name(data['name'])
    return jsonify({
        'now':timestamp(),
        'id':sensor.id,
        'name':sensor.name,
        'location':sensor.location,
        'temp':sensor.location,
        'hrel':sensor.hrel,
        'pressure':sensor.pressure,
        'weights':sensor.weights
    })


@app.route('/api/v1.0/sensors/<int:sensor_id>', methods=['DELETE'])
def api_sensors_DELETE(sensor_id):
    '''
    <int:sensor_id> : l'id du capteur
    DELETE : supprime le capteur et le détache de toutes les zones
    '''
    Sensors.remove(sensor_id)
    return jsonify({
        'now':timestamp(),
        'success':True
    })


## Pilots

@app.route('/api/v1.0/pilots', methods=['GET'])
def api_pilots_GET():
    '''
    GET : les pilotes toutes zones confondues
    '''
    data = {
        'now':timestamp(),
        'pilots': []
    }
    for pilot in Pilots :
        data['pilots'].append({
            'id':pilot.id,
            'name':pilot.name,
            'location':pilot.location,
            'logic_reverse':pilot.logic_reverse,
            'power':pilot.power
        })
    return jsonify(data)


@app.route('/api/v1.0/pilots', methods=['POST'])
def api_pilots_POST():
    '''
    POST {location:STR} : ajoute le pilote (location REQUIS adresse du
            pilote `openwifithermostat.pilot...`)
    POST {name:STR} : definit le nom du pilote. Si vide ou absent,
            récupéré sur le pilote
    '''
    #TODO à tester
    data,errors = check_data(request.get_json(force=True),[
        ('location',{'type':str,'require':True}),
        ('name',{'type':str}),
    ])
    if errors :
        return jsonify({'errors':errors})
    if not data['name'] :
        data['name'] = UdpDevices.get(data['location']).get_datum('name')
    pilot = Pilots.add(name=data['name'],location=data['location'])
    return jsonify({
        'now':timestamp(),
        'id':pilot.id,
        'name':pilot.name,
        'location':pilot.location,
        'logic_reverse':pilot.logic_reverse,
        'power':pilot.power
    })


@app.route('/api/v1.0/pilots/<int:pilot_id>', methods=['PUT'])
def api_pilots_PUT(pilot_id):
    '''
    <int:pilot_id> : l'id du pilote
    PUT {name} : modifie le nom si vide ou absent, récupéré sur le
            pilote
    '''
    data,errors = check_data(request.get_json(force=True),[
        ('name',{'type':str}),
    ])
    if errors :
        return jsonify({'errors':errors})
    pilot = Pilots.get(pilot_id)
    if data['name'] :
        pilot.set_name(data['name'])
    return jsonify({
        'now':timestamp(),
        'id':pilot.id,
        'name':pilot.name,
        'location':pilot.location,
        'logic_reverse':pilot.logic_reverse,
        'power':pilot.power
    })


@app.route('/api/v1.0/pilots/<int:pilot_id>', methods=['DELETE'])
def api_pilots_DELETE(pilot_id):
    '''
    <int:pilot_id> : l'id du pilote
    DELETE : supprime le pilote et le détache de toutes les zones
    '''
    Pilots.remove(pilot_id)
    return jsonify({
        'now':timestamp(),
        'success':True
    })


@app.route('/api/v1.0/udp/devices/<path:location_start>', methods=['GET'])
def udp_devices_GET(location_start):
    '''
    <path:location_start> : un début d'adresse
    GET : les périphériques (capteurs, pilotes) commencant par cette
            adresse
    '''
    data = []
    for udpdevice in UdpDevices.search(location_start) :
        data.append({
            'location':udpdevice.get_location(),
            'data':udpdevice.get_data()
            })
    return jsonify({'devices':data})


## Période d'absence

@app.route('/api/v1.0/away', methods=['GET','PUT'])
def put_away_GET():
    '''
    GET : retourne la période d'absence
    PUT {from:TIMESTAMP} : modifie le début de la période d'absence
    PUT {to:TIMESTAMP} : modifie la fin de la période d'absence
    '''
    if request.method == 'PUT' :
        data,errors = check_data(request.get_json(force=True),[
            ('from',{'type':int}),
            ('to',{'type':int})
        ])
        if errors :
            return jsonify({'errors':errors})
        if data['from'] :
            Cfg.set('away_from',datetime.fromtimestamp(data['from']))
        if data['to'] :
            Cfg.set('away_to',datetime.fromtimestamp(data['to']))
        for zone in Zones :
            zone.reload(reload_schedule=True, reload_name=False)
    return jsonify({
        'from': timestamp(Cfg.away_from),
        'to': timestamp(Cfg.away_to)
    })


## Historique

@app.route('/api/v1.0/history', methods=['GET'])
def history_GET():
    '''
    GET : retourne l'historique
    ?from:TIMESTAMP : à partir de (par défaut il y a une semaine)
    &to:TIMESTAMP : jusqu'à (par défaut aujourd'hui)
    &cols:temp,hrel,pressure,power : les valeurs de (par défaut toutes)
    &zones:1,2,.. : id(s) des zones (séparateur "," par défaut toutes)
    &average:[daily|houry|none] : moyenne par journées, heures ou aucune
    automatique par défaut (>60j: 'daily', >7j: 'houry', <=7j: 'none')
    '''
    data = {}
    if 'from' in request.args :
        data['from'] = datetime.fromtimestamp(float(request.args['from']))
    else :
        data['from'] = datetime.now() - timedelta(days=7)
    if 'to' in request.args :
        data['to'] = datetime.fromtimestamp(float(request.args['to']))
    else :
        data['to'] = None
    if 'cols' in request.args :
        data['cols'] = request.args['cols'].split(',')
    else:
        data['cols'] = None
    if 'zones' in request.args :
        data['zones'] = request.args['zones'].split(',')
    else:
        data['zones'] = None
    if 'average' in request.args :
		data['average'] = request.args['average']
    else:
        data['average'] = None
    history = []
    for h_entry in History.get(dt_from=data['from'], dt_to=data['to'], cols=data['cols'], zones=data['zones'], average=data['average']) :
        history.append((timestamp(h_entry[0]),)+h_entry[1:])
    return jsonify({
        'now':timestamp(),
        'history': history
    })


#unit

@app.route('/api/v1.0/help', methods=['GET'])
def unit_GET():
    '''
    GET : titres, unitée des différentes clefs utilisées
    '''
    return jsonify({
        'now':{'title':'Date','unit':'second','comment':'timestamp'},
        'temp':{'title':'Température','unit':'°C','comment':'Précision 1/10ème'},
        'hrel':{'title':'Humiditée relative','unit':'%','comment':''},
        'pressure':{'title':'Pression','unit':'hPa','comment':''},
        'away_temp':{'title':'Tempéraure en absence','unit':'°C','comment':''},
        'week_second': {'title':'Seconde de la semaine','unit':'second','comment':'Lundi 00:00:00 = 0'},
        'weight': {'title':'Pondération','unit':'-','comment':'Facteur d\'importance pour une moyenne pondérée (d\'un capteur pour un zone par exemple)'}
    });


@app.route('/proxy/<path:url>')
def proxy(url):
    '''
    <url> : du contenu à afficher.
    '''
    req = requests.get(url, stream = True)
    return Response(stream_with_context(req.iter_content()), content_type = req.headers['content-type'])


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        #~ debug=True
    )

