#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


if __name__ == '__main__':
    import __init__
    exit()

import requests, logging
import Const, UdpDevices, DB
from time import time
from threading import Timer


class Pilots (list) :
    '''
    Liste des pilots
    '''

    def __init__(self) :
        DB.c.execute("SELECT id FROM pilots")
        for row in DB.c.fetchall() :
            self.append(Pilot().load(row[0]))

    def get(self,id) :
        '''
        Cherche un pilot
          int id: id du pilot
        Return: object Pilot
        '''
        for pilot in self:
            if pilot.id == id:
                return pilot
        return None


    def get_by_location(self,location) :
        '''
        Recupère le pilot avec l'adresse
          str location: l'adresse (openwifithermostat.pilot...)
        Return: object pilot
        '''
        for sensor in self :
            if sensor.location == location:
                return sensor
        return None


    def add(self, location, name=None, logic_reverse=False) :
        '''
        Ajout un pilot
          str name: nom du pilot
          str location: uid ou url de l'api
        Return: object Pilot
        '''
        pilot = self.get_by_location(location) or Pilot().new(name=name, location=location, logic_reverse=logic_reverse)
        self.append(pilot)
        return pilot


    def remove(self,id) :
        '''
        Supprime le pilot
          int id: id du pilot à supprimé
        Return: list pilots
        '''
        pilot = self.get(id)
        pilot.remove()
        super(Pilots,self).remove(pilot)
        return self


class Pilot :
    '''
    Un pilot
      int id: id
      str name: nom
      bool power: état
      dict power_by_zone: état des zones rattachées {id_zone: état, ...}
      str location: adresse (openwifithermostat.pilot...)
      object UdpDevice: les infos fournis via udp
      bool logic_reverse: inverser la logique (étindre le pilot si la
            température est trop basse. True pour inverser
    '''

    def __init__(self) :
        self.id = None
        self.name = None
        self.power = None
        self.power_by_zone = {}
        self.location = None
        self.UdpDevice = None
        self.logic_reverse = False


    def new(self, location, name=None, logic_reverse=False) :
        '''
        Créer un pilot
          str name: nom du pilot
          location: uid ou url de l'api
        Return: object Pilot
        '''
        self._get_UdpDevice()
        self._load_power()
        DB.c.execute("INSERT INTO pilots (name,location,logic_reverse) VALUES (?,?,?)", [name, location, logic_reverse])
        DB.conn.commit()
        self.id = DB.c.lastrowid
        self.name = name
        self.location = location
        self.logic_reverse = False
        return self


    def load(self, id) :
        '''
        Charge un pilot anciennement créé
          int id: id du capteur
        Return: object Pilot
        '''
        self.id = id
        DB.c.execute("SELECT name,location,logic_reverse FROM pilots WHERE id = ?", [id])
        (self.name, self.location,self.logic_reverse) = DB.c.fetchone()
        self._get_UdpDevice()
        self._load_power()
        return self


    def set_name(self, name) :
        '''
        Change le nom
          str name: nom
        Return: nouveau nom
        '''
        DB.c.execute("UPDATE pilots SET name = ? WHERE id = ?",[name, self.id])
        DB.conn.commit()
        self.name = name
        return self.name


    def set_logic_reverse(self, logic_reverse) :
        '''
        Logic inverse (True inverse la logique)
          bool logic_reverse: inverser la logique
        Return: valeur logic inverse
        '''
        DB.c.execute("UPDATE pilots SET logic_reverse = ? WHERE id = ?",[logic_reverse, self.id])
        DB.conn.commit()
        self.logic_reverse = logic_reverse
        return self.logic_reverse


    def register(self,zone_id) :
        '''
        Enregistrer une zone
          int zone_id: id de la zone
        '''
        if zone_id in self.power_by_zone :
            logging.warning("pilot %s already registred for zone %s"%(self.location,zone_id))
            return False
        self.power_by_zone[zone_id] = False
        DB.c.execute("INSERT INTO zones_pilots (zone_id, pilot_id) VALUES (?,?)", [zone_id, self.id])
        DB.conn.commit()
        return self.power_by_zone


    def unregister(self,zone_id) :
        '''
        Désenregistrer une zone
          int zone_id: id de la zone
        '''
        self.power_by_zone.pop(zone_id)
        DB.c.execute("DELETE FROM zones_pilots WHERE zone_id = ? AND pilot_id = ?", [zone_id, self.id])
        DB.conn.commit()


    def set_power(self, zone_id, power) :
        '''
        Définit l'état du pilot pour une zone
          int zone_id: l'id de la zone
          bool power: état
        Return: nouvelle état générale
        '''
        if self.UdpDevice :
            self.power_by_zone[zone_id] = power
            power = False
            for (zone_id,z_power) in self.power_by_zone.items() :
                #plusieur zone peuvent etre connecté, il suffit que l'une soit active
                if z_power == True:
                    power=True
                    break
            self.power = power
            try :
                if self.logic_reverse :
                    r = requests.post(self.UdpDevice.data['api_url']+'v1.0',json={'power':not power})
                    self.power = not r.json()['power']
                else :
                    r = requests.post(self.UdpDevice.data['api_url']+'v1.0',json={'power':power}) #requests bloque tout la 1ère fois, un ctrl+C et après ça passe
                    self.power = r.json()['power']
            except :
                logging.error("Pilots: Can't access %s"%self.location)
            return self.power
        elif time() > Const.timestart + 35 :
            #démaré depuis plus de 35 seconde et toujours aucun udpDevice
            logging.error("Pilots: %s no found"%self.location)


    def get_power(self, verify=False) :
        '''
        Etat du pilot
          bool verify: contacte le capteur et vérfie son état [default:False]
        Return: état
        '''
        if verify :
            r = requests.get(self.url_api+'v1.0')
            if self.logic_reverse :
                self.power = r.json()['power']
            else :
                self.power = r.json()['power']
        return self.power


    def remove(self):
        '''
        Supprimer le capteur
        Return: True
        '''
        DB.c.execute("DELETE FROM zones_pilots WHERE zone_id = ?", [self.id])
        DB.c.execute("DELETE FROM pilots WHERE id = ?", [self.id])
        DB.conn.commit()
        return True


    def _load_power(self) :
        '''
        Charger les zones ratachée
        '''
        DB.c.execute("SELECT zone_id FROM zones_pilots WHERE pilot_id = ?", [self.id])
        for row in DB.c.fetchall() :
            self.power_by_zone[int(row[0])] = False


    def _get_UdpDevice(self) :
        '''
        Recupère les infos udp
        '''
        self.UdpDevice = UdpDevices.get(self.location)
        if not self.UdpDevice :
            logging.info("%s no found, retry in 15 seconds"%(self.location))
            timer = Timer(15, self._get_UdpDevice, ())
            timer.setDaemon(1)
            timer.start()
