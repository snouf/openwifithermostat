var days = ['dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi'];
var days_mondayIsDay0 = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche'];
var days_abbr = ['di','lu','ma','me','je','ve','sa'];
var days_order = [1,2,3,4,5,6,0];
var days_order_mondayIsDay0 = [0,1,2,3,4,5,6];
var months = ['janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre'];
var months_abbr = ['janv','fév.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'];
$.prototype.clickdown = function (cb) {
    var timeout;
    function repeat() {
        cb();
        timeout = setTimeout(repeat,100);
    }
    this.click(function() {cb();});
    this.mousedown(function(){timeout = setTimeout(repeat,500);});
    this.mouseup(function(){clearTimeout(timeout);});
    this.mouseleave(function(){clearTimeout(timeout);});
    return this;
};
$.prototype.daySelector = function (value,mondayIsDay0) {
    if(mondayIsDay0 ==true ) {
        var _days_order = days_order_mondayIsDay0;
        var _days = days_mondayIsDay0;
    } else {
        var _days_order = days_order_mondayIsDay0;
        var _days = days_mondayIsDay0;
    }
    var $this;
    function _update() {
        $this.value = [];
        for (d = 0; d < 7; ++d) {
            if ($days[d].prop('checked')) {
                $this.value.push(_days_order[d]);
            }
        }
    }
    $this = $(this);
    $this.value = value;
    var $controlgroup = $('<fieldset data-role="controlgroup"></fieldset>')
    $this.html($controlgroup)
    var $days = [];
    for (d = 0; d < 7; ++d) {
        var $day = $('<input type="checkbox">');
        var $labelday = $('<label>'+_days[_days_order[d]].toTitle()+'</label>');
        $labelday.append($day);
        if ($this.value.indexOf(_days_order[d]) != -1) {
            $day.prop('checked',true);
        }
        $day.change(function(){
            _update();
        });
        $days.push($day);
        $controlgroup.append($labelday);
    }
    $this.trigger('create');
    return $this
}
$.prototype.tempSelector = function (value) {
    var $this;
    function _update() {
        $label.html(formatTemp($this.value));
        $input.value = $this.value;
    }
    var $this = $(this);
    $this.value = value;
    var $controlgroup = $('<fieldset data-role="controlgroup" data-type="horizontal"></fieldset>')
    $this.html($controlgroup)
    var $down = $('<a href="#" data-role="button">-</a>')
    $down.click(function(){
        $this.value -= .5;
        _update();
        $down
    });
    $controlgroup.append($down);
    var $label = $('<a data-role="button" style="min-width:4em;">--.- °C</a>')
    $label.click(function(){
        $popup.popup('open',{positionTo: $(this)});
    });
    $controlgroup.append($label);
    var $up = $('<a href="#" data-role="button">+</a>')
    $up.click(function(){
        $this.value += .5;
        _update();
    });
    $controlgroup.append($up);
    var $popup = $('<div data-role="popup" />');
    var $input = $('<input type="number" data-clear-btn="true" value="" style="width:4em;"/>');
    $input.change(function() {
        $this.value = parseFloat($(this).val());
        _update();
    });
    $popup.append($input);
    $this.append($popup);
    _update();
    $this.trigger('create');
    return $this
}
$.prototype.hourSelector = function (value) {
    var $this;
    var $this = $(this);
    $this.value = value;
    var hour = Math.floor(value/3600000);
    var minute = Math.round((value/60000 - hour*60)/15)*15;
    var $controlgroup = $('<fieldset data-role="controlgroup" data-type="horizontal"></fieldset>')
    $this.html($controlgroup)
    var $hour = $('<select>');
    $controlgroup.append($hour);
    for (h = 0; h < 24; ++h) {
        if (h <= 9) {
            $hour.append($('<option value="'+h+'">0'+h+'</option>'));
        } else {
            $hour.append($('<option value="'+h+'">'+h+'</option>'));
        }
    }
    $hour.val(hour);
    $hour.change(function(){
        hour = parseFloat($(this).val());
        $this.value = (hour * 60 + minute) * 60000;
    });
    var $minute = $('<select>');
    $controlgroup.append($minute);
    for (m = 0; m < 60; m+=15) {
        if (h <= 9) {
            $minute.append($('<option value="'+m+'">0'+m+'</option>'));
        } else {
            $minute.append($('<option value="'+m+'">'+m+'</option>'));
        }
    }
    $minute.val(minute);
    $minute.change(function(){
        minute = parseFloat($(this).val());
        $this.value = (hour * 60 + minute) * 60000;
    });
    $this.trigger('create');
    return $this
}
Date.prototype.formatHour = (function (hour, minute, second, separator) {
    if(hour === undefined) hour = true;
    if(minute === undefined) minute = true;
    if(second === undefined) second = false;
    if(separator === undefined) separator = ':';
    function fill0 (num) {
        if (num <= 9) {
            return '0'+num.toFixed(0);
        } else {
            return num.toFixed(0);
        }
    }
    ret = [];
    if (hour) {
        ret.push(fill0(this.getUTCHours()));
    }
    if (minute) {
        ret.push(fill0(this.getUTCMinutes()));
    }
    if (second) {
        ret.push(fill0(this.getUTCSeconds()));
    }
    return ret.join(separator);
});
Number.prototype.formatPressure = function(precision) {
    if(precision === undefined) precision = 0;
    return this.toFixed(precision)+' hPa';
};
Number.prototype.formatHrel = function(precision) {
    if(precision === undefined) precision = 1;
    return this.toFixed(precision)+' %';
};
Number.prototype.formatTemp = function(precision) {
    if(precision === undefined) precision = 1;
    return this.toFixed(precision)+' °C';
};
String.prototype.toTitle = function() {
    return this[0].toUpperCase()+this.substr(1)
};

function formatPressure (pressure, prec) {
    if (pressure != null) {
        return pressure.formatPressure(prec);
    } else {
        return '---- hPa';
    }
}
function formatHrel (hrel, prec) {
    if (hrel != null) {
        return hrel.formatHrel(prec);
    } else {
        return '--.- %';
    }
}
function formatTemp (temp, prec) {
    if (temp != null) {
        return temp.formatTemp(prec);
    } else {
        return '--.- °C';
    }
}
var timer_update_sidebar;
function update_sidebar() {
    $.getJSON( '/api/v1.0/zones', function( data ) {
        for (i = 0; i < data.zones.length; ++i) {
            $('#nav-sidebar-temp-'+data.zones[i].id).html(formatTemp(data.zones[i].temp));
        }
    });
}
if (localStorage.extensions) {
    extensions = JSON.parse(localStorage.extensions);
} else {
    extensions = [];
}
for (i = 0; i < extensions.length; ++i) {
    if (page_url.search(RegExp(extensions[i].available_url)) != -1)
        $('head').append('<script src="/extensions/'+extensions[i].dirname+'/script.js"/>');
}
$(document).ready(function(){
    $('#nav-sidebar').panel({
        beforeopen:function(){
            update_sidebar();
            timer_update_sidebar = setInterval(update_sidebar, 5000);
        },
        close:function(){
            clearInterval(timer_update_sidebar);
        }
    });
    $.getJSON( '/api/v1.0/zones', function( data ) {
        $sidebar_zone = $('#sidebar-zones');
        $sidebar_zone.html('');
        for (i = 0; i < data.zones.length; ++i) {
            $('#sidebar-zones').append($('<li><a href="/zones/'+data.zones[i].id+'">'+data.zones[i].name+'<span id="nav-sidebar-temp-'+data.zones[i].id+'" class="ui-li-count">'+formatTemp(data.zones[i].temp)+'</span></a></li>'));
        }
        $sidebar_zone.listview('refresh');
        $('#nav-sidebar').find('a').each(function(){
            var $this = $(this);
            if ((page_url) & ($this.attr('href').indexOf(page_url) == 0)) $this.addClass('ui-btn-active')
        });
    });
});
