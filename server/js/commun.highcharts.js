Highcharts.setOptions({
    global: {
        useUTC: false
    },
    lang: {
        weekdays: days
    },
    credits: {
        enabled: false
    },
})
function toGraphSeries(data) {
    var series = [];
    if (data.length >= 1) {
        for (j = 0; j < data[0].length-1; ++j) {
            series[j] = [];
        }
    }
    for (i = 0; i < data.length; ++i) {
        date = data[i][0]*1000;
        for (j = 0; j < data[0].length-1; ++j) {
            if (data[i][j+1]) {
                series[j].push([date,data[i][j+1]]);
            }
        }
    }
    return series
}
$.prototype.tinyhighcharts = function(ops) {this.highcharts($.extend(true,{
        chart:{
            type:'area',
            margin: [12,0,10,0]
        },
        title:{
            text:null
        },
        xAxis: {
            title:{
                text:null
            },
            type: 'datetime',
            tickInterval: 24*3600000, //jour
            tickWidth: 0,
            gridLineWidth: 1,
            labels: {
                align: 'left',
                x: 3,
                y: -3
            },
            dateTimeLabelFormats: {
                day: '%A'
            },
        },
        yAxis: {
            title:{
                text:null
            },
            allowDecimals:false,
            opposite: true,
            labels: {
                align: 'right',
                x:-3,
                y:-3
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            xDateFormat: '%Hh%M',
            pointFormat:'<b>{point.y}</b>'
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            },
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(.3).get('rgba')]
                    ]
                },
                threshold: null
            }
        }
    },ops));
};
