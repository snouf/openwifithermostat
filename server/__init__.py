#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


import sys, re, logging
from time import time

class _const:

    class ConstError(TypeError):
        pass

    def __setattr__(self,name,value):
        if self.__dict__.has_key(name):
            raise self.ConstError, "Can't rebind const(%s)" % name
        self.__dict__[name] = value


sys.modules['Const'] = _const()
import Const
Const.timestart = time()

import version as Version
sys.modules['Version'] = Version

from database import DB
DB = DB()
sys.modules['DB'] = DB

db_struct = DB.c.execute("SELECT value FROM config WHERE key = 'db_struct' ").fetchone()[0]
version = DB.c.execute("SELECT value FROM config WHERE key = 'version' ").fetchone()[0]
if version != Version.version :
    logging.warning(u"La version du programme dans la base de donnée (%s) ne correspond pas à la version de programme lancée (%s). Lancez update.py pour mettre à jour le numéro de version."%(version,Version.version))
if db_struct != Version.db_struct :
    logging.error(u"La structure de la base de donnée (%s) ne correspond pas à celle requise (%s). Faites une sauvegarde de la base de donnée et lancez update.py pour mettre à jour."%(db_struct,Version.db_struct))
    sys.exit(1)

from config import Cfg
sys.modules['Cfg'] = Cfg()

from history import History
sys.modules['History'] = History()

from udpdevices import UdpDevices
sys.modules['UdpDevices'] = UdpDevices()

from modes import Modes
sys.modules['Modes'] = Modes()

from sensors import Sensors
sys.modules['Sensors'] = Sensors()

from pilots import Pilots
sys.modules['Pilots'] = Pilots()

from zones import Zones
sys.modules['Zones'] = Zones()

