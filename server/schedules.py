#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

if __name__ == '__main__':
    import __init__
    exit()

import DB, Const, Cfg, Modes
from datetime import datetime, timedelta
import logging
logging.basicConfig(level=logging.DEBUG)


class Schedules :
    '''
    Programmation hebdomadaire
      int zone_id: id de la zone concernée
    '''

    def __init__(self, zone_id) :
        self.zone_id = zone_id


    def add_one(self, week_seconds, temp) :
        '''
        Ajoute (ou remplace) un changement de température
          float week_seconds: seconde le la semaine (sera arrondie au
                1/4 d'heure
          float temp: la température en °C (serra arrondi au 1/10éme)
        Return: tuple (week_seconds, temp) avec valeur arrondi
        '''
        week_seconds = round(week_seconds/(15*60))*15*60 #arrondi au 1/4 d'heure
        temp = round(temp,1)
        DB.c.execute("DELETE FROM schedules WHERE zone_id = ? AND mode_id = ? AND week_second = ?", [self.zone_id, Modes.active.id, week_seconds])
        DB.c.execute("INSERT INTO schedules (zone_id, mode_id, temp, week_second) VALUES ( ? , ? , ? , ? )", [self.zone_id, Modes.active.id, temp, week_seconds])
        DB.conn.commit()
        return week_seconds, temp


    def remove_one(self, week_seconds) :
        '''
        Supprime un chagement de température
          float week_seconds: seconde de la semaine (sera arrondie au
                1/4 d'heure
        '''
        week_seconds = round(week_seconds/(15*60))*15*60 #arrondi au 1/4 d'heure
        DB.c.execute("DELETE FROM schedules WHERE zone_id = ? AND mode_id = ? AND week_second = ?", [self.zone_id, Modes.active.id, week_seconds])
        DB.conn.commit()
        return week_seconds


    def load_default(self) :
        '''
        Charge des valeurs par defaut
        '''

        DB.c.execute("DELETE FROM schedules WHERE zone_id = ? AND mode_id = ?", [self.zone_id, Modes.active.id])
        if Modes.active.id == 1 :
            '''default'''
            for day in range(7) : #tous les jours
                for (temp,week_seconds) in [
                    (20.,timedelta(days=day,hours=8).total_seconds()),
                    (16.,timedelta(days=day,hours=23).total_seconds())
                ] : #à 8h 20°C, à 23h 16°C
                    DB.c.execute("INSERT INTO schedules (zone_id, mode_id, temp, week_second) VALUES ( ? , ? , ? ,? )", [self.zone_id, Modes.active.id, temp, week_seconds])
        else :
            '''on copie le 1er mode'''
            DB.c.execute("SELECT week_second, temp FROM schedules WHERE zone_id = ? AND mode_id = 1", [self.zone_id])
            for row in DB.c.fetchall():
                DB.c.execute("INSERT INTO schedules (zone_id, mode_id, week_second, temp) VALUES ( ? , ? , ? , ?)", [self.zone_id, Modes.active.id, row[0], row[1]])
        DB.conn.commit()


    def cur (self,dt=None) :
        '''
        Plage de programmation en cours
          datetime dt: date pour la recherche, None = maintenant,
                [defaut: None]
        Return: tuple (datetime début, temp)
        '''
        if dt==None :
            dt=datetime.now()

        if Cfg.away_from < dt < Cfg.away_to :
            '''Nous somme en période d'absence'''
            DB.c.execute("SELECT away_temp FROM zones WHERE id = ?", [self.zone_id])
            return Cfg.away_from , DB.c.fetchone()[0]

        week_timedelta = self._dt_to_week_timedelta(dt)
        week_seconds = week_timedelta.total_seconds()
        DB.c.execute("SELECT temp, week_second FROM schedules WHERE zone_id = ? AND mode_id = ? AND week_second <= ? ORDER BY week_second DESC LIMIT 1", [self.zone_id, Modes.active.id, week_seconds])
        row = DB.c.fetchone()
        if row :
            (cur_temp, cur_week_seconds) = row
            cur_dt = dt - week_timedelta + timedelta(seconds=cur_week_seconds)
        else :
            'avant le debut de semaine'
            DB.c.execute("SELECT temp, week_second FROM schedules WHERE zone_id = ? AND mode_id = ? ORDER BY week_second DESC LIMIT 1", [self.zone_id, Modes.active.id])
            row = DB.c.fetchone()
            if row :
                (cur_temp, cur_week_seconds) = row
            else :
                '''pas de data'''
                (cur_temp, cur_week_seconds) = (Cfg.default_temp,week_seconds)
            #on cherche la date du meme jours de la semaine d'avant
            cur_dt = dt - week_timedelta + timedelta(weeks=-1, seconds=cur_week_seconds)

        if cur_dt < Cfg.away_to < dt :
            '''Précédent changement de température signifie fin d'une période d'absence'''
            cur_dt = Cfg.away_to

        return cur_dt, cur_temp


    def next (self,dt=None) :
        '''
        Prochaine plage de programmation
          datetime dt: date pour la recherche, None = maintenant,
                [defaut: None]
        Return: tuple (datetime début, temp)
        '''
        if dt==None :
            dt=datetime.now()

        from database import DB
        DB = DB()

        logging.debug("get next sheduler for zone id %i"%self.zone_id)

        if Cfg.away_from <= dt < Cfg.away_to :
            '''Nous somme en période d'absence'''
            logging.debug('Nous somme en période d\'absence')
            week_timedelta = self._dt_to_week_timedelta(Cfg.away_to)
            week_seconds = week_timedelta.total_seconds()
            #température à la fin de la période de vacances
            DB.c.execute("SELECT temp FROM schedules WHERE zone_id = ? AND mode_id = ? AND week_second <= ? ORDER BY week_second LIMIT 1", [self.zone_id, Modes.active.id, week_seconds])
            row = DB.c.fetchone()
            if row :
                next_temp = row[0]
            else :
                '''la température se trouve après la fin de la semaine'''
                DB.c.execute("SELECT temp, week_second FROM schedules WHERE zone_id = ? AND mode_id = ? ORDER BY week_second DESC LIMIT 1", [self.zone_id, Modes.active.id])
                row = DB.c.fetchone()
                if row :
                    next_temp = row[0]
                else :
                    next_temp = Cfg.default_temp
            return Cfg.away_to, next_temp

        week_timedelta = self._dt_to_week_timedelta(dt)
        week_seconds = week_timedelta.total_seconds()
        DB.c.execute("SELECT temp, week_second FROM schedules WHERE zone_id = ? AND mode_id = ? AND week_second > ? ORDER BY week_second LIMIT 1", [self.zone_id, Modes.active.id, week_seconds])
        row = DB.c.fetchone()
        if row :
            (next_temp, next_week_seconds) = row
            next_dt = dt - week_timedelta + timedelta(seconds=next_week_seconds)
        else :
            '''après fin de semaine'''
            logging.debug('Prochain changement semaine prochaine')
            DB.c.execute("SELECT temp, week_second FROM schedules WHERE zone_id = ? AND mode_id = ? ORDER BY week_second LIMIT 1", [self.zone_id, Modes.active.id])
            row = DB.c.fetchone()
            if row :
                (next_temp, next_week_seconds) = row
            else :
                '''pas de data'''
                (next_temp, next_week_seconds) = (Cfg.default_temp,week_seconds)
            #on cherche la date du meme jours de la semaine d'après
            next_dt = dt - week_timedelta + timedelta(weeks=1, seconds=next_week_seconds)

        if dt < Cfg.away_from < next_dt :
            '''Prochain changement de température signifie début d'une période d'absence'''
            DB.c.execute("SELECT away_temp FROM zones WHERE id = ?", [self.zone_id])
            away_temp = DB.c.fetchone()[0]
            return Cfg.away_from,away_temp
        else :
            return next_dt, next_temp


    def weekly(self):
        '''
        Les plages de programmation pour toute la semaine
        Return: list de tuples (week_seconds, température en °C)
        '''
        DB.c.execute("SELECT week_second, temp FROM schedules WHERE zone_id = ? AND mode_id = ? ORDER BY week_second", [self.zone_id, Modes.active.id])
        return DB.c.fetchall()


    def _dt_to_week_timedelta(self,dt) :
        '''
        Convertir un datetime en timedelta depuis le début de semaine
        '''
        return timedelta(days=dt.weekday(), hours=dt.hour, minutes=dt.minute, seconds=dt.second, microseconds=dt.microsecond)
