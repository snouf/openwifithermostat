#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


if __name__ == '__main__':
    import __init__
    exit()

import Cfg, DB, History, Sensors, Pilots, Const
from schedules import Schedules
from datetime import datetime
from time import time
from threading import Timer
import logging


class Zones (list) :


    def __init__(self) :
        '''
        Liste des zones
        '''
        DB.c.execute("SELECT id FROM zones")
        for row in DB.c.fetchall() :
            self.append(Zone().load(row[0]))


    def get(self,id) :
        '''
        Retourne une zone
          int id: id de la zone
        '''
        for zone in self :
            if zone.id == id:
                return zone
        return None


    def get_all(self) :
        '''
        Retourne toutes les zones
        '''
        return self


    def add(self, name, away_temp=Cfg.default_temp,speed_temp_climb=0.) :
        '''
        Ajouter une zone
          str name: Le nom de ma zone
          float away_temp: température en absence (default_temp par
                défaut)
          float speed_temp_climb: vitesse de montée en température en nb
                de secondes pour 1°C. 0 pour désactiver le fonction, (0
                par défaut)
        '''
        zone = Zone().new(name=name,away_temp=away_temp,speed_temp_climb=speed_temp_climb)
        self.append(zone)
        return zone


    def remove(self,id) :
        '''
        Supprime une zone
          int id: l'id de la zone
        '''
        zone = self.get(id)
        zone.remove()
        super(Zones,self).remove(zone)
        return self



class Zone :
    '''
    Une zone
      int id: id
      str name: nom
      list sensor: les capteurs
      list pilot: les pilots
      float temp: la température moyenne en °C (None si pas de données)
      float hrel: l'humidité moyenne en % (None si pas de données)
      float pressure: la pression moyenne en hpa (None si pas de
            données)
      float power: état de la zone (true : chauffer, false arret
            chauffage)
      float speed_temp_climb : vitesse de montée de la température (nb
            secondes pour +1°)
      float last_temp: les X dernières messure de température
      float last_power_change_timer: la dernière fois que l'état
            (allumé/éteint) à été changé
      tuple cur_schedule: (datetime début de cette température,
            température actuelle)
      tuple next_schedule: (datetime prochain changement, prochaine
            température)
      object Schedules: le programmeur hebdomadaire
    '''

    def __init__(self) :
        self.id = None
        self.name = None
        self.sensors = []
        self.pilots = []
        self.temp = None
        self.hrel = None
        self.pressure = None
        self.power = None
        self.speed_temp_climb = 0.
        self.last_temp = []
        self.last_power_change_timer = 0.
        self.away_temp = Cfg.default_temp
        self.cur_schedule = (datetime.fromtimestamp(0),0.) # (date, temp)
        self.normal_cur_schedule = (datetime.fromtimestamp(0),0.) # (date, temp)
        self.next_schedule = (datetime.fromtimestamp(0),0.) # (date, temp)
        self.normal_next_schedule = (datetime.fromtimestamp(0),0.) # (date, temp)
        self.Schedules = None


    def new (self,name,away_temp=Cfg.default_temp,speed_temp_climb=0.) :
        '''
        Nouvelle zone
          str name: nom
          float away_temp: température en absence (default_temp par
                défaut)
          float speed_temp_climb: vitesse de montée en température en nb
                de secondes pour 1°C. 0 pour désactiver le fonction, (0
                par défaut)
        '''
        DB.c.execute("INSERT INTO zones (name,away_temp,speed_temp_climb) VALUES (?,?,?)", [name,away_temp,speed_temp_climb])
        DB.conn.commit()
        self.id = DB.c.lastrowid
        self.name = name
        self.away_temp = away_temp
        self.speed_temp_climb = speed_temp_climb
        self.Schedules = Schedules(self.id)
        self.Schedules.load_default()
        self.next_schedule = self.Schedules.cur() #sera déplacé dans cur_schedule lors de self._load_pilots()
        self._start_deamon()
        return self


    def load (self,id) :
        '''
        Charge une zone existante
          int id: l'id de la zone
        '''
        self.id = id
        self.Schedules = Schedules(self.id)
        self.next_schedule = self.Schedules.cur() #sera déplacé dans cur_schedule lors de self._update_pilots()
        DB.c.execute("SELECT name,away_temp,speed_temp_climb FROM zones WHERE id = ?", [self.id])
        self.name, self.away_temp, self.speed_temp_climb = DB.c.fetchone()
        self._load_pilots()
        self._load_sensors()
        self._start_deamon()
        return self


    def reload(self,reload_schedule=True, reload_name=True) :
        '''
        Recharge la zone
          bool reload_schedule: recharge la programmation hebdomadaire
                [defaut: True]
          bool reload_name: recharge le nom [defaut: True]
        '''
        if reload_schedule :
            self.next_schedule = self.Schedules.cur() #sera déplacé dans cur_schedule lors de self._update_pilots()
            self._update_pilots() #pas obligatoire mais si on ne le fait pas il faut attendre jusqu'à 5 seconde pour que ce soit effectif
        if reload_name :
            DB.c.execute("SELECT name FROM zones WHERE id = ?", [self.id])
            self.name = DB.c.fetchone()[0]
        return self


    def remove(self):
        '''
        Supprime cette zone
        '''
        for sensor in self.sensors :
            sensor.unregister(self.id)
        for pilot in self.pilots:
            pilot.unregister(self.id)
        DB.c.execute("DELETE FROM zones WHERE id = ?", [self.id])
        DB.conn.commit()
        return True


    def set_name(self,name):
        '''
        Modifier le nom
          str name: nom
        '''
        DB.c.execute("UPDATE zones SET name = ? WHERE id = ?", [name,self.id])
        DB.conn.commit()
        self.name=name
        return self


    def set_away_temp(self,away_temp):
        '''
        Modifier la température en absence
          float away_temp: température en absence °C
        '''
        DB.c.execute("UPDATE zones SET away_temp = ? WHERE id = ?", [away_temp,self.id])
        DB.conn.commit()
        self.away_temp = away_temp
        self.reload(reload_schedule=True, reload_name=False)
        return self


    def set_speed_temp_climb(self,speed_temp_climb):
        '''
        Modifier la vitesse de montée en température
          float speed_temp_climb: vitesse en nombre de secondes pour
                1°C. 0 pour désactiver le fonction.
        '''
        DB.c.execute("UPDATE zones SET speed_temp_climb = ? WHERE id = ?", [speed_temp_climb,self.id])
        DB.conn.commit()
        self.speed_temp_climb = speed_temp_climb
        return self


    def set_cur_temp(self,temp):
        '''
        Force la température jusqu'au prochaine changement de
        température
          float temp: température
        '''
        self.cur_schedule = (self.cur_schedule[0],temp)
        return self.cur_schedule


    def set_end_time(self,dt):
        '''
        Définit manuellement la fin de la teméprature en cours puis
        reprendra la température selon programation horaire
          datetime: fin de temérature
        '''
        next_temp = self.Schedules.cur(dt)[1] #température qui est désirée à dt
        self.next_schedule = (dt,next_temp)
        logging.info("next schedule : %s ; %s"%(dt.isoformat(),next_temp))
        return self.next_schedule


    def add_pilot(self,location, name=None, logic_reverse=False) :
        '''
        Ajouter un pilot
          str location: adresse du capteur (openwifithermosat.pilot...)
          str name: nom, None recupère le nom automatiquement [defaut:
                None]
          bool logic_reverse: inverse la logique de capteur (éteindre
                si température trop basse [defaut: False]
        '''
        pilot = Pilots.add(location, name=name, logic_reverse=logic_reverse)
        if pilot.register(self.id) :
            self.pilots.append(pilot)
        return pilot


    def add_sensor(self,location, name=None, weight=1) :
        '''
        Ajouter un capteur
          str location: adresse du capteur (openwifithermosat.pilot...)
          str name: nom, None recupère le nom automatiquement [defaut:
                None]
          float weight: facteur de pondération pour la moyenne [defaut:
                1]
        '''
        sensor = Sensors.add(location, name=name)
        if sensor.register(self.id,weight) :
            self.sensors.append(sensor)
        return sensor


    def remove_pilot(self,id) :
        '''
        Supprimer un pilot
          int id: id
        '''
        pilot = Pilots.get(id)
        pilot.unregister(self.id)
        self.pilots.remove(pilot)
        return pilot


    def remove_sensor(self,id) :
        '''
        Supprimer un capteur
          int id: id
        '''
        sensor = Sensors.get(id)
        sensor.unregister(self.id)
        self.sensors.remove(sensor)
        return sensor


    def _start_deamon(self) :
        '''
        Démare de deamon
        '''
        self._update_moy_sensors()
        self._update_pilots()
        if (self.temp or self.hrel or self.pressure) :
            History.append(self)
        timer = Timer(5, self._start_deamon, ())
        timer.setDaemon(1)
        timer.start()


    def _load_pilots(self) :
        '''
        Charge les pilots
        '''
        self.pilots = []
        for row in DB.c.execute("SELECT pilot_id FROM zones_pilots WHERE zone_id = ?", [self.id]) :
            self.pilots.append(Pilots.get(row[0]))


    def _load_sensors(self) :
        '''
        Charge les capteurs
        '''
        self.sensors = []
        for row in DB.c.execute("SELECT sensor_id FROM zones_sensors WHERE zone_id = ?", [self.id]) :
            self.sensors.append(Sensors.get(row[0]))


    def _update_moy_sensors(self) :
        '''
        Moyenne pondérée des capteurs
        '''
        s_temp = 0.
        s_temp_weight = 0.
        s_hrel = 0.
        s_hrel_weight = 0.
        s_pressure = 0.
        s_pressure_weight = 0.
        for sensor in self.sensors :
            weight = sensor.get_weight(self.id)
            if sensor.temp != None :
                s_temp += sensor.temp * weight
                s_temp_weight += weight
            if sensor.hrel != None :
                s_hrel += sensor.hrel * weight
                s_hrel_weight += weight
            if sensor.pressure != None :
                s_pressure += sensor.pressure * weight
                s_pressure_weight += weight

        if s_temp_weight :
            self.temp = s_temp/s_temp_weight
        else :
            self.temp = None

        if s_hrel_weight :
            self.hrel = s_hrel/s_hrel_weight
        else :
            self.hrel = None

        if s_pressure_weight :
            self.pressure = s_pressure/s_pressure_weight
        else :
            self.pressure = None


    def _update_pilots(self) :
        '''
        Capteur sur marche ou arret ?
        '''

        if datetime.now() >= self.next_schedule[0] :
            self.cur_schedule = self.next_schedule
            self.normal_cur_schedule = self.next_schedule
            self.next_schedule = self.Schedules.next()
            self.normal_next_schedule = self.next_schedule

        if self.pilots :
            now = time()

            if self.temp != None :
                self.last_temp = self.last_temp[-4:]
                self.last_temp.append(self.temp)

                temp_moy = sum(self.last_temp) / len(self.last_temp)
                logging.debug(u'Zone %s : température cible %.2f °C, moyenne dernières données %s %.2f °C, dernier changement il y a %.1f"'%(self.name, self.cur_schedule[1], self.last_temp, temp_moy,now - self.last_power_change_timer))

                if now - self.last_power_change_timer > 60 :
                    '''
                    La température à changer il y a plus d'une minute (évite
                    les mises en marche/arret inteprestif)
                    '''
                    logging.debug(u'%.2f <? %.2f ( ou %.2f <? %.2f )'%(temp_moy, self.cur_schedule[1], temp_moy, self.next_schedule[1] - ((self.next_schedule[0] - datetime.now()).total_seconds() / self.speed_temp_climb)))
                    if temp_moy < self.cur_schedule[1] or (self.speed_temp_climb and temp_moy < self.next_schedule[1] - (self.next_schedule[0] - datetime.now()).total_seconds() / self.speed_temp_climb) :
                        '''
                        la moyenne des dernières messures est sous la température voulue
                        ou sous la prochaine température voulue - dt * speed_temp_climb
                        '''
                        if self.power != True :
                            logging.info('Zone %s : pilots MARCHE'%self.name)
                            self.power = True
                            self.last_power_change_timer = now
                    else :
                        if self.power != False :
                            logging.info('Zone %s : pilots ARRET'%self.name)
                            self.power = False
                            self.last_power_change_timer = now

                for pilot in self.pilots :
                    pilot.set_power(self.id, self.power)
                return True
            else :
                return False

        return None
