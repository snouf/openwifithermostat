#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


if __name__ == '__main__':
    import __init__
    import UdpDevices
    from time import sleep
    while 1 :
        print 'wait ...'
        sleep(5)
        for udpdevice in UdpDevices :
            print udpdevice.location, udpdevice.data
        print UdpDevices


import logging
from json import loads
from socket import socket, AF_INET, SOCK_DGRAM
from time import time
from threading import Thread
logging.basicConfig(level=logging.DEBUG)


class UdpDevices (list) :
    '''
    liste des périphérique udp reçu
      object soquet: soquet
    '''

    def __init__(self) :
        self.soquet = socket(AF_INET, SOCK_DGRAM)
        self.soquet.bind(('', 5000))
        tread = Thread(target=self._on_receive)
        try :
            tread.setDaemon(True)
            tread.start()
        except (KeyboardInterrupt, SystemExit):
            cleanup_stop_thread()
            sys.exit()

    def get(self,location) :
        '''
        Recupère un périf
          str location: adresse (openwifithermostat....)
        Return: object UdpDevice
        '''
        for udpdevice in self :
            if udpdevice.location == location:
                return udpdevice
        return None

    def search(self,location_start) :
        '''
        Cherche un périf
          str location_start: début ou adresse complète (openwifithermostat....)
        Return: list object UdpDevice
        '''
        udpdevices = []
        for udpdevice in self :
            if udpdevice.location.startswith(location_start):
                udpdevices.append(udpdevice)
        return udpdevices

    def _on_receive(self) :
        '''
        A le reception d'un paquet udp
        '''
        while 1 :
            data, addr = self.soquet.recvfrom(1024)
            if data.startswith('openwifithermostat') :
                try :
                    location,data = data.split('=',1)
                    data = data.replace('<ip>',addr[0])
                    udpdevice = self.get(location)
                    if udpdevice :
                        udpdevice.set_data(data)
                    else :
                        self.append(UdpDevice(location,addr[0],data))
                except :
                    pass


class UdpDevice :
    '''
    Un périphérique reçu par udp
      str location: adresse (openwifithermostat....)
      str ip: adresse IP
      str data: les données reçu
    '''

    def __init__(self,location,ip,data=None) :
        self.location = location
        self.ip = ip
        self.set_data(data)

    def get_location(self):
        '''
        Recupère l'adresse
        Return: l'adresse (openwifithermostat....)
        '''
        return self.location

    def set_data(self,data):
        '''
        Définit les données
          str data: donnée en json
        '''
        try :
            self.data=loads(data)
        except :
            self.data=None

    def get_data(self):
        '''
        Recupère les données
        Return: dict donnée
        '''
        return self.data

    def get_datum(self,key):
        '''
        Recupère une donnée
        Return; la donnée
        '''
        if self.data.has_key(key) :
            return self.data[key]
        else :
            return None

