#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

from optparse import OptionParser
import sys, logging, os, atexit

parser = OptionParser(description="OpenWifiThermostat server")
parser.add_option("-v", "--verbose", dest="verbose", default="warning", type="string", help="Verbose (debug,info,warning,error) [warning par defaut]")
parser.add_option("-i", "--info", dest="info", action="store_true", help="Voir les infos (version, db_schema ...")
parser.add_option("-p", "--http_port", dest="http_port", type="int", default=5000, help="Port pour le serveur http(s) [5000 par défaut]")
parser.add_option("--ssl-key", dest="ssl_key", type="string", help="clef ssl pour la connection https (si non précisé démarre en http)")
parser.add_option("--ssl-cert", dest="ssl_cert", type="string", help="certificat ssl pour la connection https  (si non précisé démarre en http)")

(opts, args) = parser.parse_args()

if opts.verbose == 'debug' :
    logLevel = logging.DEBUG
elif opts.verbose == 'info' :
    logLevel = logging.INFO
elif opts.verbose == 'error' :
    logLevel = logging.ERROR
else :
    logLevel = logging.WARNING

logging.basicConfig(
    format = "%(asctime)s %(levelname)s - %(filename)s:%(lineno)d - %(message)s",
    level = logLevel
  )

if opts.info :
    import version as Version
    sys.stdout.write("OpenWifiThermostat server\n")
    sys.stdout.write("  - version: %s\n"%Version.version)
    sys.stdout.write("  - db_struct: %s\n"%Version.db_struct)
    sys.exit(0)

import __init__ as toto
import web_ui

def atexit_handler():
    import History
    History._save()
    logging.info("Exit")
    sys.exit(0)
atexit.register(atexit_handler)

if opts.ssl_key and opts.ssl_cert :
    web_ui.app.run(
        host='0.0.0.0',
        port=opts.http_port,
        ssl_context=(opts.ssl_cert,opts.ssl_key)
    )
else :
    web_ui.app.run(
        host='0.0.0.0',
        port=opts.http_port,
    )
