#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

if __name__ == '__main__':
    import __init__
    exit()

import requests, logging
import Const, UdpDevices, DB
from datetime import datetime
from time import time
from threading import Timer


class Sensors (list) :
    '''
    Liste des capteurs
    '''

    def __init__(self) :
        DB.c.execute("SELECT id FROM sensors")
        for row in DB.c.fetchall() :
            self.append(Sensor().load(row[0]))


    def get(self,id) :
        '''
        Recupère le cateur
          int id: l'id
        Return: object capteur
        '''
        for sensor in self :
            if sensor.id == id:
                return sensor
        return None


    def get_by_location(self,location) :
        '''
        Recupère le cateur avec l'adresse
          str location: l'adresse (openwifithermostat.sensor...)
        Return: object capteur
        '''
        for sensor in self :
            if sensor.location == location:
                return sensor
        return None


    def add(self, location, name=None) :
        '''
        Ajout un capteur si il n'existe pas
          str location: l'adresse (openwifithermostat.sensor...)
          str name: le nom (None pour le récupérer automatiquement)
                [defaut: None]
        Return: object capteur
        '''
        sensor = self.get_by_location(location) or Sensor().new(name=name, location=location)
        self.append(sensor)
        return sensor


    def remove(self,id) :
        '''
        Supprime un capteur
          int id: l'id
        '''
        sensor = self.get(id)
        sensor.remove()
        super(Sensors,self).remove(sensor)


class Sensor :
    '''
    Un capteur
      int id: l'id
      str name: le nom
      str location: l'adresse (openwifithermostat.sensor...)
      str UdpDevice: les infos fournient via udp
      float temp: la température en °C (None si pas de donnée)
      float hrel: l'hummidité en % (None si pas de donnée)
      float pressure: la pression en hPa (None si pas de donnée)
      float last_update: dernière mise à jour (timestamp)
      dict weights: {zone_id:weight} facteurs de pondération pour les
            zones ratachées
    '''

    def __init__(self) :
        self.id = None
        self.name = None
        self.location = None
        self.UdpDevice = None
        self.temp = None
        self.hrel = None
        self.pressure = None
        self.last_update = None
        self.weights = {}


    def new(self, location, name=None) :
        '''
        Créer un capteur
          str name: nom du pilot
          location: adresse (openwifithermostat.sensor...)
        Return: object Capteur
        '''
        self._get_UdpDevice()
        self.name = name or self.UdpDevice.data.name or location
        self.location = location
        DB.c.execute("INSERT INTO sensors (name,location) VALUES (?,?)", [self.name, location])
        DB.conn.commit()
        self.id = DB.c.lastrowid
        self._start_deamon()
        return self


    def load(self, id) :
        '''
        Charge un capteur anciennement créé
          int id: l'id
        Return: object Capteur
        '''
        self.id = id
        DB.c.execute("SELECT name,location FROM sensors WHERE id = ?", [id])
        (self.name, self.location) = DB.c.fetchone()
        self._get_UdpDevice()
        self._load_weight()
        self._start_deamon()
        return self


    def register(self,zone_id,weight=1) :
        '''
        Change le nom
          str name: nom
        Return: nouveau nom
        '''
        if zone_id in self.weights :
            logging.warning("sensor %s already registred for zone %s"%(self.location,zone_id))
            return False
        self.weights[zone_id] = weight
        DB.c.execute("INSERT INTO zones_sensors (zone_id, sensor_id, weight) VALUES (?,?,?)", [zone_id, self.id, weight])
        DB.conn.commit()
        return self.weights


    def unregister(self,zone_id) :
        '''
        Enregistrer une zone
          int zone_id: id de la zone
        '''
        self.weights.pop(zone_id)
        DB.c.execute("DELETE FROM zones_sensors WHERE zone_id = ? AND sensor_id = ?", [zone_id, str(self.id)])
        DB.conn.commit()


    def get_weight(self, zone_id) :
        '''
        Recupère le facteur de pondération pour une zone
          int zone_id: id de la zone
        Return: le facteur, 0 si zone non trouvée
        '''
        if self.weights.has_key(zone_id) :
            return self.weights[zone_id]
        else :
            return 0


    def set_name(self, name) :
        '''
        Change le nom
          str name: nom
        Return: nouveau nom
        '''
        DB.c.execute("UPDATE sensors SET name = ? WHERE id = ?",[name, self.id])
        DB.conn.commit()
        self.name = name
        return self.name


    def set_weight(self, zone_id, weight) :
        '''
        Définit le facteur de pondération pour une zone
          int zone_id: id de la zone
          float weight: le facteur
        Return: dict des facteurs
        '''
        DB.c.execute("UPDATE zones_sensors SET weight = ? WHERE zone_id = ? AND sensor_id = ?",[weight, zone_id, self.id])
        DB.conn.commit()
        self.weights[zone_id] = weight
        return self.weights


    def remove(self):
        '''
        Supprimer le capteur
        Return: True
        '''
        DB.c.execute("DELETE FROM zones_sensors WHERE sensor_id = ?", [self.id])
        DB.c.execute("DELETE FROM sensors WHERE id = ?", [self.id])
        DB.conn.commit()
        return True


    def _start_deamon(self) :
        '''
        Démarre de deamon
        '''
        self._read()
        timer = Timer(5, self._start_deamon, ())
        timer.setDaemon(1)
        timer.start()


    def _load_weight(self) :
        '''
        Charge les facteurs de pondérations
        '''
        DB.c.execute("SELECT zone_id,weight FROM zones_sensors WHERE sensor_id = ?", [self.id])
        for row in DB.c.fetchall() :
            self.weights[int(row[0])] = float(row[1])


    def _read(self) :
        '''
        Lit le capteur
        '''
        if self.UdpDevice :
            self.temp = None
            self.hrel = None
            self.pressure = None
            if self.UdpDevice.data.has_key('temp') and type(self.UdpDevice.data['temp']) in (float,int) :
                self.temp = float(self.UdpDevice.data['temp'])
            if self.UdpDevice.data.has_key('hrel') and type(self.UdpDevice.data['hrel']) in (float,int) :
                self.hrel = float(self.UdpDevice.data['hrel'])
            if self.UdpDevice.data.has_key('pressure') and type(self.UdpDevice.data['pressure']) in (float,int) :
                self.pressure = float(self.UdpDevice.data['pressure'])
            self.last_update = datetime.now()
        elif time() > Const.timestart + 35 :
            #démaré depuis plus de 35 seconde et toujours aucun udpDevice
            logging.error("%s no found"%self.location)


    def _get_UdpDevice(self) :
        '''
        Recupère les infos udp
        '''
        self.UdpDevice = UdpDevices.get(self.location)
        if not self.UdpDevice :
            logging.info("%s no found, retry in 15 seconds"%(self.location))
            timer = Timer(15, self._get_UdpDevice, ())
            timer.setDaemon(1)
            timer.start()
