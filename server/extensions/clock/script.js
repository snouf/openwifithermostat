function update_clock() {
    var date = new Date;
    $('#clock').html(days[date.getDay()].toTitle()+' '+date.getDate()+' '+months[date.getMonth()]+' '+date.getFullYear()+', '+date.formatHour());
    setTimeout(update_clock,60000-date.getSeconds()*1000-date.getMilliseconds());
}
$(document).ready(function(){
     $('.ui-header').after('<div id="clock" style="text-align:right"></div>');
    update_clock();
});
