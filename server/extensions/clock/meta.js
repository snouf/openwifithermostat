meta={
    name:'Horloge',
    description:'Affiche une horloge (date et heure) sous la barre de titre.',
    version:'0.20160131',
    author:'Jonas',
    licence:'GNU GPL V2',
    website:'https://gitlab.com/snouf/openwifithermostat/tree/master/server/extensions/clock',
    dirname:'clock',
    available_url:'.*',//a RegExp
    setup : false,
    install : function() {
        return true;
    },
    uninstall : function() {
        return true;
    }
}
