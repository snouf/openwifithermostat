function meteofrance_update_sidebar() {
    var now = Date.now();
    for (i = 0; i < meteofrance_citysid.length; ++i) {
        if (now - sessionStorage.getItem('extensions.meteofrance.'+meteofrance_citysid[i]+'.last_update') > 15*60000) {//maj toutes les 15'
            $.ajax({
                url:'/proxy/http://www.meteo-france.mobi/ws/getDetail/france/'+meteofrance_citysid[i]+'.json'
            }).done(function (data) {
                sessionStorage.setItem('extensions.meteofrance.'+data.result.ville.indicatif+'.cache',JSON.stringify(data));
                sessionStorage.setItem('extensions.meteofrance.'+data.result.ville.indicatif+'.last_update',Date.now());
                $('#sidebar-meteofrance-'+data.result.ville.indicatif+'-temp').html(data.result.resumes['0_resume'].temperatureMin+'/'+data.result.resumes['0_resume'].temperatureMax+' °C');
            });
        } else {
            data = JSON.parse(sessionStorage.getItem('extensions.meteofrance.'+meteofrance_citysid[i]+'.cache'));
            $('#sidebar-meteofrance-'+data.result.ville.indicatif+'-temp').html(data.result.resumes['0_resume'].temperatureMin+'/'+data.result.resumes['0_resume'].temperatureMax+' °C');
        }
    }
}
if (JSON.parse(localStorage.getItem('extensions.meteofrance.citysid')))
    var meteofrance_citysid = JSON.parse(localStorage.getItem('extensions.meteofrance.citysid'));
else
    var meteofrance_citysid = [];
$(document).ready(function(){
    $('#nav-sidebar').each(function(){
        $('#sidebar-history').after('<h3>MétéoFrance</h3><ul id="sidebar-meteofrance" data-role="listview" data-inset="true"><ul>');
        $('#sidebar-meteofrance').listview();
        var now = Date.now();
        for (i = 0; i < meteofrance_citysid.length; ++i) {
            try {
                data = JSON.parse(sessionStorage.getItem('extensions.meteofrance.'+meteofrance_citysid[i]+'.cache'));
                $('#sidebar-meteofrance').append('<li><a href="/extensions/meteofrance/weather.html?ville='+data.result.ville.indicatif+'">'+data.result.ville.nom+' ('+data.result.ville.codePostal+')<span id="sidebar-meteofrance-'+data.result.ville.indicatif+'-temp" class="ui-li-count">'+data.result.resumes['0_resume'].temperatureMin+'/'+data.result.resumes['0_resume'].temperatureMax+' °C</span></a></li>');
            } catch (e) {
                $.ajax({
                    url:'/proxy/http://www.meteo-france.mobi/ws/getDetail/france/'+meteofrance_citysid[i]+'.json'
                }).done(function (data) {
                    sessionStorage.setItem('extensions.meteofrance.'+data.result.ville.indicatif+'.cache',JSON.stringify(data));
                    sessionStorage.setItem('extensions.meteofrance.'+data.result.ville.indicatif+'.last_update',Date.now());
                    $('#sidebar-meteofrance').append('<li><a href="/extensions/meteofrance/weather.html?ville='+data.result.ville.indicatif+'">'+data.result.ville.nom+' ('+data.result.ville.codePostal+')<span id="sidebar-meteofrance-'+data.result.ville.indicatif+'-temp" class="ui-li-count">'+data.result.resumes['0_resume'].temperatureMin+'/'+data.result.resumes['0_resume'].temperatureMax+' °C</span></a></li>');
                    $('#sidebar-meteofrance').listview('refresh');
                });
            }
            $('#sidebar-meteofrance').listview('refresh');
        }
        meteofrance_update_sidebar();
        setInterval(meteofrance_update_sidebar,5*60*1000);//actualisations toutes les 5 minutes
    });
});
