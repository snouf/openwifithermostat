var meta = {
    name:'MétéoFrance',
    description:'La météo en france (MétéoFrance)',
    version:'0.20160207',
    author:'Jonas',
    licence:'GNU GPL V2',
    website:'https://gitlab.com/snouf/openwifithermostat/tree/master/server/extensions/meteofrance',
    dirname:'meteofrance',
    available_url:'.*',
    setup : function() {
        document.location.href='/extensions/meteofrance/setup.html';
    },
    install : function() {
        return true;
    },
    uninstall : function() {
        localStorage.removeItem('extensions.meteofrance.citysid');
        return true;
    }
}
