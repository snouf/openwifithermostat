function openweathermap_update_sidebar() {
    var now = Date.now();
    for (i = 0; i < openweathermap_citysid.length; ++i) {
        if (now - sessionStorage.getItem('extensions.openweathermap.'+openweathermap_citysid[i]+'.current_conditions.last_update') > 15*60000) {//maj toutes les 15'
            $.ajax({
                url:'http://api.openweathermap.org/data/2.5/weather?id='+openweathermap_citysid[i]+'&appid='+localStorage.getItem('extensions.openweathermap.apikey')+'&units=metric&lang=fr',
                crossDomain: true
            }).done(function (data) {
                sessionStorage.setItem('extensions.openweathermap.'+data.id+'.current_conditions.cache',JSON.stringify(data));
                sessionStorage.setItem('extensions.openweathermap.'+data.id+'.current_conditions.last_update',Date.now());
                $('#sidebar-openweathermap-'+data.id+'-temp').html(data.main.temp.toFixed(1)+' °C');
            });
        } else {
            console.log('extensions.openweathermap.'+openweathermap_citysid[i]+'.current_conditions du cache');
            data = JSON.parse(sessionStorage.getItem('extensions.openweathermap.'+openweathermap_citysid[i]+'.current_conditions.cache'));
            $('#sidebar-openweathermap-'+data.id+'-temp').html(data.main.temp.toFixed(1)+' °C');
        }
    }
}
if (JSON.parse(localStorage.getItem('extensions.openweathermap.citysid')))
    var openweathermap_citysid = JSON.parse(localStorage.getItem('extensions.openweathermap.citysid'));
else
    var openweathermap_citysid = [];
$(document).ready(function(){
    $('#nav-sidebar').each(function(){
        $('#sidebar-history').after('<h3>Météo</h3><ul id="sidebar-openweathermap" data-role="listview" data-inset="true"><ul>');
        $('#sidebar-openweathermap').listview();
        var now = Date.now()
        for (i = 0; i < openweathermap_citysid.length; ++i) {
            if (now - sessionStorage.getItem('extensions.openweathermap.'+openweathermap_citysid[i]+'.current_conditions.last_update') > 15*60000) {//maj toutes les 15'
                $.ajax({
                    url:'http://api.openweathermap.org/data/2.5/weather?id='+openweathermap_citysid[i]+'&appid='+localStorage.getItem('extensions.openweathermap.apikey')+'&units=metric&lang=fr',
                    crossDomain: true
                }).done(function (data) {
                    sessionStorage.setItem('extensions.openweathermap.'+data.id+'.current_conditions.cache',JSON.stringify(data));
                    sessionStorage.setItem('extensions.openweathermap.'+data.id+'.current_conditions.last_update',Date.now());
                    $('#sidebar-openweathermap').append('<li><a href="/extensions/openweathermap/weather.html?city='+data.id+'">'+data.name+' ('+data.sys.country+')<span id="sidebar-openweathermap-'+data.id+'-temp" class="ui-li-count">'+data.main.temp.toFixed(1)+' °C</span></a></li>');
                    $('#sidebar-openweathermap').listview('refresh');;
                });
            } else {
                console.log('extensions.openweathermap.'+openweathermap_citysid[i]+'.current_conditions du cache');
                data = JSON.parse(sessionStorage.getItem('extensions.openweathermap.'+openweathermap_citysid[i]+'.current_conditions.cache'));
                $('#sidebar-openweathermap').append('<li><a href="/extensions/openweathermap/weather.html?city='+data.id+'">'+data.name+' ('+data.sys.country+')<span id="sidebar-openweathermap-'+data.id+'-temp" class="ui-li-count">'+data.main.temp.toFixed(1)+' °C</span></a></li>');
            }
            $('#sidebar-openweathermap').listview('refresh');
        }
        setInterval(openweathermap_update_sidebar,5*60*1000);//actualisations toutes les 5 minutes
    });
});
