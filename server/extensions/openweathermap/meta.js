var meta = {
    name:'OpenWeatherMap',
    description:'La météo d\'une ville de n\'importe quels pays.',
    version:'0.20160131',
    author:'Jonas',
    licence:'GNU GPL V2',
    website:'https://gitlab.com/snouf/openwifithermostat/tree/master/server/extensions/openweathermap',
    dirname:'openweathermap',
    available_url:'.*',
    setup : function() {
        document.location.href='/extensions/openweathermap/setup.html';
    },
    install : function() {
        return true;
    },
    uninstall : function() {
        localStorage.removeItem('extensions.openweathermap.apikey');
        localStorage.removeItem('extensions.openweathermap.citysid');
        return true;
    }
}
