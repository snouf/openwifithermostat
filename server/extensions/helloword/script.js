$(document).ready(function(){
    var name = localStorage.getItem('extensions.helloword.name')
    if (!name) name = 'word';
    $('#nav-sidebar div').prepend('\
        <div class="ui-body ui-body-a ui-corner-all">\
            Hello '+name+'\
        </div>'
    );
});
