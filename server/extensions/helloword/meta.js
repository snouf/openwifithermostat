meta={
    name:'Hello Word',
    description:'Affiche Hello ... dans le menu de la page d\'acceuil et dans le menu des zones',
    version:'0.20160124',
    author:'Jonas',
    licence:'GNU GPL V2',
    website:'https://gitlab.com/snouf/openwifithermostat/tree/master/server/extensions/helloword',
    dirname:'helloword',
    available_url:'^(/|/zones/[0-9]+)$',//a RegExp
    setup : function() {
        document.location.href='/extensions/helloword/setup.html';
    },
    install : function() {
        return true;
    },
    uninstall : function() {
        localStorage.removeItem('extensions.helloword.name');
        return true;
    }
}
