#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


import DB

class Cfg :
    '''
    Configuration
      str version: version de openwifithermost
      str db_struct: version de la structure de la base de donnée
      datetime dt_away_to: fin d'une période d'absence
      datetime dt_away_from: début d'une période d'absence
      int mode_id: mode actif
    '''
    def __init__(self) :
        self.reload()

    def reload(self) :
        '''
        Recharger la configuration depuis la base de donnée
        '''
        #version
        self.version = DB.c.execute("SELECT value FROM config WHERE key = 'version' ").fetchone()[0]
        self.db_struct = DB.c.execute("SELECT value FROM config WHERE key = 'db_struct' ").fetchone()[0]

        #température par défaut (si schedules vide)
        self.default_temp = float(DB.c.execute("SELECT value FROM config WHERE key = 'default_temp' ").fetchone()[0])

        #en absence
        self.away_to = DB.c.execute("SELECT value as '[timestamp]' FROM config WHERE key = 'away_to' ").fetchone()[0]
        self.away_from = DB.c.execute("SELECT value as '[timestamp]' FROM config WHERE key = 'away_from' ").fetchone()[0]

        #Mode
        self.mode_id = int(DB.c.execute("SELECT value FROM config WHERE key = 'mode_id' ").fetchone()[0])

        #Historique
        self.history_entry_every = int(DB.c.execute("SELECT value FROM config WHERE key = 'history_entry_every' ").fetchone()[0])
        self.history_save_at = int(DB.c.execute("SELECT value FROM config WHERE key = 'history_save_at' ").fetchone()[0])


    def set(self,key,val):
        '''
        Modifié une clef de configuration
          str key: la cref
          str|float|int|datetime: la valeur
        '''
        DB.c.execute("UPDATE config SET value = ? WHERE key = ? ",[ val, key ])
        DB.conn.commit()
        setattr(self, key, val)

    def get(self,key):
        '''
        Recupère une clef
          str key: la cref
        '''
        getattr(self, key)
