/*
 * This file is a part of OpenWifiThermostat
 * OpenWifiThermostat This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */


#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <DHT.h>
#include <EEPROM.h>
#include "lib/EEPROMAnything.h"
#include <WiFiUdp.h>
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson

#define DHTPIN 2
#define DHTTYPE AM2301

#define SENSOR_NAME_EEPROM_ADDR 0
#define SENSOR_NAME_LENGTH 100

char sensor_name[SENSOR_NAME_LENGTH];
char wifi_ssid[32];
char wifi_pswd[100];

ESP8266WebServer server(80);
DHT dht(DHTPIN, DHTTYPE, 11);
WiFiUDP Udp;

float hrel, temp;
String webString = "", cmd = "", opt = "", val = "";
byte mac[6];
char addr_mac[128];
StaticJsonBuffer<200> jsonBuffer;
char jBuffer[256];
JsonObject& JsonApi = jsonBuffer.createObject();
JsonObject& JsonUdp = jsonBuffer.createObject();
JsonObject& JsonApiHelp = jsonBuffer.createObject();
unsigned long previousMillis = 0;        // will store last temp was read
unsigned long UDPtimer = 0;
const long interval = 2000;              // interval at which to read sensor


void gettemperature() {
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;

    hrel = dht.readHumidity();
    temp = dht.readTemperature();

    if (isnan(hrel) || isnan(temp)) {
      Serial.println("Failed to read from AM3201 sensor!");
      return;
    }
  }
}

void wifi_connect() {
    // Connect to WiFi network
  WiFi.begin(wifi_ssid, wifi_pswd);
  Serial.println(wifi_ssid);
  Serial.println(wifi_pswd);
  Serial.print("Working to connect");

  // Wait for connection
  unsigned long currentMillis = millis();
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (millis() > currentMillis+10000) {
      Serial.println("Abord (timeout)");
      break;
    }
  }

  if (WiFi.status() == WL_CONNECTED) {
    WiFi.macAddress(mac);
    (
      String(mac[5],HEX)+':'+
      String(mac[4],HEX)+':'+
      String(mac[3],HEX)+':'+
      String(mac[2],HEX)+':'+
      String(mac[1],HEX)+':'+
      String(mac[0],HEX)
      ).toCharArray(addr_mac,17);
    Serial.println("");
    Serial.print("openwifithermostat ");
    Serial.println(sensor_name);
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    JsonApi["api_url"] = "http://<ip>/api/";
    Serial.print("Mac: ");
    Serial.println(addr_mac);

  }
}

void wifi_disconnect() {
  WiFi.disconnect();
  Serial.print("Disconnected");
}

void setup() {
  Serial.begin(9600);
  dht.begin();
  EEPROM.begin(512);

  EEPROM_readAnything(SENSOR_NAME_EEPROM_ADDR, sensor_name);
  wifi_connect();

  server.on("/", [](){
    gettemperature();
    char* webString = "<!DOCTYPE html>\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n<head>\n<title></title>\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n<link rel=\"stylesheet\" href=\"http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css\" />\n<script src=\"http://code.jquery.com/jquery-1.11.3.min.js\"></script>\n<script src=\"http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js\"></script>\n<script type=\"text/javascript\">\n//<![CDATA[\nvar help = {};\nconst captors = ['temp','hrel','pressure'];\nfunction update () {\n$.getJSON( \"api/v1.0\", function( data ) {\nfor (i = 0; i < captors.length; ++i) {\nif (data[captors[i]]) {\n$(\"#\" + captors[i]).html(data[captors[i]].toFixed(1));\n}\n}\n});\n}\nString.prototype.toTitle = function () {\nreturn this.charAt(0).toUpperCase() + this.substr(1);\n}\n$(document).ready(function(){\n$.getJSON( \"api/v1.0/help\", function( help ) {\n$.getJSON( \"api/v1.0\", function( data ) {\n$(\"title\").html(data.name.toTitle());\n$(\"h1\").html(data.name.toTitle());\n$content = $(\"#content\");\nfor (i = 0; i < captors.length; ++i) {\nif (data[captors[i]]) {\n$content.append(\n$('<div class=\"ui-bar ui-bar-a\"><h3>' + help[captors[i]].title.toTitle() + '</h3></div><div class=\"ui-body ui-body-a\"><span class=\"value\" id=\"' + captors[i] + '\">'+data[captors[i]].toFixed(1)+'</span> <span class=\"unit\">'+help[captors[i]].unit+'</span></div>')\n);\n}\n}\nsetInterval(update,2500);\n});\n});\n});\n//]]>\n</script>\n<style>\n.value, .unit {\nfont-size : 5em;\nmargin:0;\n}\n</style>\n</head>\n<body>\n<div data-role=\"page\">\n<div data-role=\"header\">\n<h1></h1>\n</div>\n<div role=\"main\" class=\"ui-content\">\n<div id=\"content\" class=\"ui-corner-all custom-corners\">\n</div>\n</div>\n</div>\n</body>\n</html>";
    server.send(200, "application/xhtml+xml", webString);
  });

  server.on("/api/v1.0", [](){
    gettemperature();
    if (isnan(hrel)) {
      JsonApi["hrel"] = static_cast<const char*>(NULL);
    } else {
      JsonApi["hrel"] = hrel;
    }
    if (isnan(temp)) {
      JsonApi["temp"] = static_cast<const char*>(NULL);
    } else {
      JsonApi["temp"] = temp;
    }
    JsonApi.printTo(jBuffer,sizeof(jBuffer));
    server.send(200, "application/json", jBuffer);
  });

  server.on("/api/v1.0/help", [](){
    gettemperature();
    webString= //TODO use JsonHelp
        "{\"temp\":{\"title\":\"température\",\"unit\":\"°C\",\"comment\":\"\"},\"hrel\":{\"title\":\"humiditée relative\",\"unit\":\"%\",\"comment\":\"\"}}";
    server.send(200, "application/json", webString);            // send to someones browser when asked
  });

  server.on("/temp", [](){
    gettemperature();
    webString=String((int)temp)+" °C";
    server.send(200, "text/plain", webString);
  });

  server.on("/hrel", [](){
    gettemperature();
    webString=String((int)hrel)+" %";
    server.send(200, "text/plain", webString);
  });

  JsonApi["name"] = sensor_name;
  JsonUdp["name"] = sensor_name;
  JsonUdp["api_url"] = static_cast<const char*>(NULL);
  JsonArray& api_version = JsonUdp.createNestedArray("api_version");
  api_version.add("v1.0");

  Udp.begin(5000);
  server.begin();
  Serial.println("HTTP server started");
}



void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      cmd.trim();
      opt.trim();
      val.trim();
      if (cmd == "help") {
        Serial.println("connect [<ssid> <password>]");
        Serial.println("disconnect");
        Serial.println("reconnect");
        Serial.println("get name");
        Serial.println("get ssid");
        Serial.println("get ip");
        Serial.println("get temp");
        Serial.println("get hrel");
        Serial.println("set name <sensor_name>");
      } else if (cmd == "get") {
        if (opt == "name") {
          Serial.println(sensor_name);
        } else if (opt == "ssid") {
          Serial.println(wifi_ssid);
        } else if (opt == "ip") {
          Serial.println(WiFi.localIP());
        } else if (opt == "temp") {
          Serial.println(temp);
        } else if (opt == "hrel") {
          Serial.println(hrel);
        } else {
          Serial.println("Can't get "+opt+". Type help");
        }
      } else if (cmd == "set") {
        if (opt == "name") {
          val.toCharArray(sensor_name,SENSOR_NAME_LENGTH);
          EEPROM_writeAnything(SENSOR_NAME_EEPROM_ADDR, sensor_name);
          Serial.print("set name ");
          Serial.println(sensor_name);
          JsonApi["name"] = sensor_name;
          JsonUdp["name"] = sensor_name;
        } else {
          Serial.println("Can't set "+opt+". Type help");
        }
      } else if (cmd == "connect") {
        opt.toCharArray(wifi_ssid,32);
        val.toCharArray(wifi_pswd,100);
        wifi_connect();
      } else if (cmd == "disconnect") {
        wifi_disconnect();
      } else if (cmd == "reconnect") {
        wifi_disconnect();
        wifi_connect();
      } else {
        Serial.println("Unknow command \""+cmd+"\". Type help");
      }
      cmd = "";
      opt = "";
      val = "";
    } else if ((val != "") || ((opt != "") && (inChar == ' '))) {
      val += inChar;
    } else if ((opt != "") || ((cmd != "") && (inChar == ' '))) {
      opt += inChar;
    } else {
      cmd += inChar;
    }
  }
}

void loop(void)
{
  serialEvent();
  server.handleClient();
  if (UDPtimer < millis()) {
    //Broadcast
    Serial.println("send discovery udp packet");
    gettemperature();

    Serial.print("UdpBegin");
    Udp.beginPacket("255.255.255.255", 5000);
    Serial.print("...");
    Udp.write("openwifithermostat.sensor.esp8266.");
    Udp.write(addr_mac);
    Udp.write("=");
    Serial.print("=");
    if (isnan(hrel)) {
      JsonUdp["hrel"] = static_cast<const char*>(NULL);
    } else {
      JsonUdp["hrel"] = hrel;
    }
    if (isnan(temp)) {
      JsonUdp["temp"] = static_cast<const char*>(NULL);
    } else {
      JsonUdp["temp"] = temp;
    }
    JsonUdp.printTo(jBuffer,sizeof(jBuffer));
    Udp.write(jBuffer);
    Serial.println(jBuffer);
    Serial.println(".");
    Udp.endPacket();
    UDPtimer = millis()+5000;
  }
}
