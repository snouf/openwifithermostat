#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

from os.path import dirname
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from optparse import OptionParser
from sys import stdout
import logging
from threading import Timer
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST, gethostbyname, gethostname
from uuid import getnode as get_mac
from json import loads, dumps

class BaseSensorHandler(BaseHTTPRequestHandler):

    def __init__(self,*args):
        BaseHTTPRequestHandler.__init__(self,*args)


    def do_HEAD(self):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()


    def do_GET(self):
        global last_read, temp, hrel

        if self.path == '/' :
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            with open(dirname(__file__)+'/template.html', 'r') as f:
                self.wfile.write(f.read())
            f.closed
        elif self.path == '/api/v1.0' :
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            data = {"name":opts.name}
            data.update(get_data())
            self.wfile.write(dumps(data))
        elif self.path == '/api/v1.0/help' :
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(dumps(get_help()))
        else :
            self.send_error(404)


opts = None
args = None
end_uid = 'base'
api_url = 'http://<ip>:%i/api/'
api_version = ['1.0']
_data = {}
_help = {
    'temp':{'title':'Température','unit':'°C','comment':''},
    'hrel':{'title':'Humiditée relative','unit':'%','comment':''},
    'pressure':{'title':'Pression','unit':'Pa','comment':''}
}
httpHandler = BaseSensorHandler


def get_data() :
    '''
    Toggle power pilot [bool] return new power
    '''
    global _data
    return _data


def get_help() :
    '''
    Toggle power pilot [bool] return new power
    '''
    help = {}
    for (key,val) in _help.items():
        if _data.has_key(key):
            help[key] = val
    return help


def parse_args():
    global opts, args
    (opts, args) = parser.parse_args()
    return (opts, args)


def start_broadcast() :
    data = {
        'name':opts.name,
        'data':get_data(),
        'api_url':api_url,
        'api_version':api_version,
        'temp':None
    }
    data.update(get_data())
    try :
        s.sendto("openwifithermostat.sensor.%s.%s=%s"%(mac,end_uid,dumps(data)), ('<broadcast>', 5000))
    except Exception,e :
        logging.error(e.message)
    Timer(5, start_broadcast).start()


def start_httpserver() :
    httpd = HTTPServer(("", opts.port), httpHandler)
    print "Start http server %s at %s:%i"%(opts.name,ip,opts.port)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()


def init():
    parse_args()
    start_broadcast()
    start_httpserver()


parser = OptionParser(
    description="Base sensor for openWifiThermostat"
)
parser.add_option("-n", "--name", dest="name",default="base pilot", type="string", help="pilot name [default: %default]")
parser.add_option("-p", "--port", dest="port",default=8082, type="int", help="port http [default: %default]")
#~ parser.add_option("-a", "--auth", dest="port",default="localhost|192.168.*", type="string", help="authorised host (regular expression) [default: %default]")

s = socket(AF_INET, SOCK_DGRAM) #create UDP socket
s.bind(('', 0))
s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1) #this is a broadcast socket
ip= gethostbyname(gethostname())
mac = ':'.join(("%012X" % get_mac())[i:i+2] for i in range(0, 12, 2))
