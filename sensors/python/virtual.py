#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import _baseSensor
from random import random

_baseSensor.end_uid = 'virtual'
_baseSensor.parser.set_defaults(name='Virtual')
_baseSensor.parser.set_description("A virtual sensor (random values for temp, hrel and pressure)")

def get_data() :
    _baseSensor._data =  {
        "temp":random()*30,
        "hrel":random()*20 + 40,
        "pressure":random()*10 + 995,
    }
    return _baseSensor._data
_baseSensor.get_data = get_data


if __name__ == '__main__' :
    _baseSensor.init()




