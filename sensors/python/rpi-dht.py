#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import _baseSensor
from time import time
try :
    import Adafruit_DHT
except :
    raise "Adafruit_DHT module needed https://github.com/adafruit/Adafruit_Python_DHT/"
from json import dumps

_baseSensor.parser.set_defaults(name='Am2301 on Raspberry Pi')
_baseSensor.parser.set_description("A Am2301 sensor on a Raspberry Pi")
_baseSensor.parser.add_option("-g", "--gpio", dest="gpio", type="int", help="[REQUIRE] GPIO for am2301 data wire")

last_read = 0

def get_data() :
    global last_read
    cur_read = time()
    if last_read < cur_read - 2 :
        (_baseSensor._data['hrel'],_baseSensor._data['temp']) = Adafruit_DHT.read_retry(Adafruit_DHT.AM2302, _baseSensor.opts.gpio)
        last_read = cur_read
    return _baseSensor._data
_baseSensor.get_data = get_data


if __name__ == '__main__' :
    (opts, args) = _baseSensor.parser.parse_args()
    if not opts.gpio :
        _baseSensor.parser.error('GPIO require')

    _baseSensor.end_uid = 'rpi.am2301.gpio%i'%opts.gpio
    _baseSensor.init()
