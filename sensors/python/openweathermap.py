#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is a part of OpenWifiThermostat
#  OpenWifiThermostat is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import _baseSensor
import requests
from time import time

last_get = 0

def get_data() :
    global last_get
    now = time()
    if now - last_get >= 15*60 : #Ne renouvelle la requête qu'après 1/4 d'heure
        r = requests.get('http://api.openweathermap.org/data/2.5/weather',params={
            'id':opts.city_id,
            'appid':opts.api_key,
            'units':'metric'
        }).json()
        _baseSensor._data = {
            'temp':r['main']['temp'],
            'hrel':r['main']['humidity'],
            'pressure':r['main']['pressure'],
        }
        last_get = now
        cur_read = time()
    return _baseSensor._data
_baseSensor.get_data = get_data


if __name__ == '__main__' :
    _baseSensor.parser.set_description("openweathermap sensor (get temp, hrel, pressure from )")
    _baseSensor.parser.add_option("-k", "--api_key", dest="api_key", type="string", help="[REQUIRE] openweathermap api_key (http://openweathermap.org/appid)")
    _baseSensor.parser.add_option("-c", "--city_id", dest="city_id", type="string", help="[REQUIRE] openweathermap city_id (http://openweathermap.org/find)")
    (opts, args) = _baseSensor.parser.parse_args()
    if not opts.city_id or not opts.api_key   :
        _baseSensor.parser.error('city_id and api_key require')
    try :
        r = requests.get('http://api.openweathermap.org/data/2.5/weather',params={
                'id':opts.city_id,
                'appid':opts.api_key
            }).json()
    except :
        print 'module requests >= 2.9 require'
        exit(1)
    _baseSensor.end_uid = 'openweathermap.%s'%r['id']
    _baseSensor.parser.set_defaults(name='%s (%s) from openweathermap'%(r['name'], r['sys']['country']))
    _baseSensor.init()
