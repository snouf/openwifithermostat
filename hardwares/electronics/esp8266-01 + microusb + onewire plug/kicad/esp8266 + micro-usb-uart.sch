EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:esp8266 + micro-usb-uart-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "esp8266-OnWire"
Date "2015-11-22"
Rev "v1.0"
Comp "(c) Jonas <snouf@mythtv-fr.org>"
Comment1 "option) any later version."
Comment2 "as published by the Free Software Foundation; either version 2 of the License, or (at your "
Comment3 "you can redistribute it and/or modify it under the terms of the GNU General Public License"
Comment4 "esp8266-OneWire is a part of openwifithermostat"
$EndDescr
$Comp
L LD1117S33TR U1
U 1 1 56178312
P 4550 3300
F 0 "U1" H 4300 3500 40  0000 C CNN
F 1 "LT1129IST-3.3" H 4700 3500 40  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 4550 3400 35  0000 C CIN
F 3 "" H 4550 3300 60  0000 C CNN
	1    4550 3300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P1
U 1 1 563F2657
P 5000 5550
F 0 "P1" H 5000 5750 50  0000 C CNN
F 1 "OneWire Plug" V 5100 5550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 4500 5950 60  0001 C CNN
F 3 "" H 5000 5550 60  0000 C CNN
	1    5000 5550
	0    -1   1    0   
$EndComp
$Comp
L CONN_01X06 P2
U 1 1 563F5033
P 3450 4300
F 0 "P2" H 3450 4650 50  0000 C CNN
F 1 "USBUART" V 3550 4300 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06" H 3450 4300 60  0001 C CNN
F 3 "" H 3450 4300 60  0000 C CNN
	1    3450 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4250 4250 3650 4250
Wire Wire Line
	4250 3900 4250 4250
Wire Wire Line
	4000 3250 4150 3250
$Comp
L CONN_01X01 P3
U 1 1 5640E436
P 4400 5550
F 0 "P3" H 4400 5650 50  0000 C CNN
F 1 "GPIO0" V 4500 5550 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Square-TH_Small" H 4400 5550 60  0001 C CNN
F 3 "" H 4400 5550 60  0000 C CNN
	1    4400 5550
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 P4
U 1 1 5640E57F
P 4150 5550
F 0 "P4" H 4150 5650 50  0000 C CNN
F 1 "GND" V 4250 5550 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Square-TH_Small" H 4150 5550 60  0001 C CNN
F 3 "" H 4150 5550 60  0000 C CNN
	1    4150 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3550 4550 3600
Wire Wire Line
	3650 4150 3700 4150
Wire Wire Line
	4900 5350 4850 5350
$Comp
L VCC #PWR01
U 1 1 56426E2A
P 6200 3900
F 0 "#PWR01" H 6200 4000 30  0001 C CNN
F 1 "VCC" H 6200 4000 30  0000 C CNN
F 2 "" H 6200 3900 60  0000 C CNN
F 3 "" H 6200 3900 60  0000 C CNN
	1    6200 3900
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR02
U 1 1 56426E39
P 6200 4250
F 0 "#PWR02" H 6200 4350 30  0001 C CNN
F 1 "VCC" H 6200 4350 30  0000 C CNN
F 2 "" H 6200 4250 60  0000 C CNN
F 3 "" H 6200 4250 60  0000 C CNN
	1    6200 4250
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR03
U 1 1 56426E48
P 5000 3250
F 0 "#PWR03" H 5000 3350 30  0001 C CNN
F 1 "VCC" H 5000 3350 30  0000 C CNN
F 2 "" H 5000 3250 60  0000 C CNN
F 3 "" H 5000 3250 60  0000 C CNN
	1    5000 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 3250 5000 3250
NoConn ~ 6200 4050
Wire Wire Line
	4000 4050 3650 4050
Wire Wire Line
	4000 3250 4000 4050
Wire Wire Line
	3650 4350 3800 4350
Wire Wire Line
	3800 4350 3800 4700
$Comp
L GND #PWR?
U 1 1 5647A96B
P 4550 3600
F 0 "#PWR?" H 4550 3350 50  0001 C CNN
F 1 "GND" H 4550 3450 50  0000 C CNN
F 2 "" H 4550 3600 60  0000 C CNN
F 3 "" H 4550 3600 60  0000 C CNN
	1    4550 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5647A9DB
P 4150 5300
F 0 "#PWR?" H 4150 5050 50  0001 C CNN
F 1 "GND" H 4150 5150 50  0000 C CNN
F 2 "" H 4150 5300 60  0000 C CNN
F 3 "" H 4150 5300 60  0000 C CNN
	1    4150 5300
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 5647AA08
P 4850 5300
F 0 "#PWR?" H 4850 5050 50  0001 C CNN
F 1 "GND" H 4850 5150 50  0000 C CNN
F 2 "" H 4850 5300 60  0000 C CNN
F 3 "" H 4850 5300 60  0000 C CNN
	1    4850 5300
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 5647AA53
P 5300 4400
F 0 "#PWR?" H 5300 4150 50  0001 C CNN
F 1 "GND" H 5300 4250 50  0000 C CNN
F 2 "" H 5300 4400 60  0000 C CNN
F 3 "" H 5300 4400 60  0000 C CNN
	1    5300 4400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5647AADF
P 3700 4150
F 0 "#PWR?" H 3700 3900 50  0001 C CNN
F 1 "GND" H 3700 4000 50  0000 C CNN
F 2 "" H 3700 4150 60  0000 C CNN
F 3 "" H 3700 4150 60  0000 C CNN
	1    3700 4150
	0    -1   -1   0   
$EndComp
$Comp
L VCC #PWR?
U 1 1 5647AD1C
P 5000 5300
F 0 "#PWR?" H 5000 5150 50  0001 C CNN
F 1 "VCC" H 5000 5450 50  0000 C CNN
F 2 "" H 5000 5300 60  0000 C CNN
F 3 "" H 5000 5300 60  0000 C CNN
	1    5000 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 5350 4150 5300
$Comp
L ESP8266-01 U2
U 1 1 56178272
P 5750 4150
F 0 "U2" H 5750 4050 50  0000 C CNN
F 1 "ESP-01V090" H 5750 4250 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x04" H 5750 4150 50  0001 C CNN
F 3 "" H 5750 4150 50  0001 C CNN
	1    5750 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4250 5100 5350
Wire Wire Line
	5100 4250 5300 4250
Wire Wire Line
	4400 4050 4400 5350
Wire Wire Line
	4850 5350 4850 5300
Wire Wire Line
	5000 5350 5000 5300
Text GLabel 3300 4050 0    47   Input ~ 0
5V
Text GLabel 3300 4150 0    47   Input ~ 0
GND
Text GLabel 3300 4250 0    47   Input ~ 0
TXO
Text GLabel 3300 4350 0    47   Input ~ 0
RXI
Text GLabel 3300 4450 0    47   Input ~ 0
DTR
Text GLabel 3300 4550 0    47   Input ~ 0
3V3
Wire Wire Line
	5300 3900 4250 3900
Wire Wire Line
	4400 4050 5300 4050
Wire Wire Line
	6200 4400 6200 4700
Wire Wire Line
	6200 4700 3800 4700
$EndSCHEMATC
